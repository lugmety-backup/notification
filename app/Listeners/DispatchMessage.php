<?php

namespace App\Listeners;

use App\Events\SendNotification;
use App\Http\Controllers\NotificationClass\CustomPushNotification;
use App\Http\Controllers\NotificationClass\EmailNotification;
use App\Http\Controllers\NotificationClass\SmsNotification;
use Bschmitt\Amqp\Amqp;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;
use Settings;
use Illuminate\Support\Facades\File;

class DispatchMessage implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * $tries holds no. of tries queue make before assigning a job as failed job
     * @var int
     */
    public $tries = 1;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $event;

    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExampleEvent $event
     * @return void
     */
    public function handle(SendNotification $event)
    {
        $this->event = $event;
        $data = $this->event->array;
        try {
//            if (!$this->overrideMailConfig()) {
//                throw new \Exception("Mail config could not be overridden");
//            }

//            if (!$this->overrideFcmConfig()) {
//                throw new \Exception("Fcm config could not be overridden");
//            }
            if (!$this->overrideEnv()) {
                throw new \Exception("Env could not be overridden");
            }

        } catch (\Exception $ex) {
            Log::info(json_encode($ex->getMessage()));
        }


        try {
            if ($data["notification_type"] == "email") {
                $email = new EmailNotification($data);
            } elseif ($data["notification_type"] == "sms") {
                $email = new SmsNotification($data);
            }elseif ($data['notification_type'] == "push-notification"){
                $custom = new CustomPushNotification($data);
            }
            //Log::info(json_encode($this->event->array));
        } catch (\Exception $ex) {
            $this->failed([
                'error' => 'error'
            ]);
        }

    }

    public function failed($exception)
    {
        Log::info(serialize($exception));


    }

    private function overrideMailConfig()
    {
        try {
            $data = Settings::getSettings('mail-driver');
            if ($data["status"] == 200) {
                $mail['driver'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-host');
            if ($data["status"] == 200) {
                $mail['host'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-server-username');
            if ($data["status"] == 200) {
                $mail['username'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-server-password');
            if ($data["status"] == 200) {
                $mail['password'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-server-port');
            if ($data["status"] == 200) {
                $mail['port'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-encryption');
            if ($data["status"] == 200) {
                $mail['encryption'] = empty($data['message']['data']['value']) ? null : $data['message']['data']['value'];
            }
            $data = var_export($mail, 1);
            if (File::put(base_path() . '/config/mail.php', "<?php\n return $data ;")) {
                return true;
            }
        } catch (\Exception $ex) {
            Log::info(json_encode($ex->getMessage()));
            return false;
        }


    }

    private function overrideFcmConfig()
    {
        try {
            $fcm['driver'] = 'http';
            $fcm['log_enabled'] = false;
            $fcm['http']['server_send_url'] = 'https://fcm.googleapis.com/fcm/send';
            $fcm['http']['server_group_url'] = 'https://android.googleapis.com/gcm/notification';
            $fcm['http']['timeout'] = 30.0;
            $data = Settings::getSettings('fcm-server-key');
            if ($data["status"] == 200) {
                $fcm['http']['server_key'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-sender-id');
            if ($data["status"] == 200) {
                $fcm['http']['sender_id'] = $data['message']['data']['value'];
            }

//

            $data = var_export($fcm, 1);
            if (File::put(base_path() . '/config/fcm.php', "<?php\n return $data ;")) {
                return true;
            }
        } catch (\Exception $ex) {
            Log::info(json_encode($ex->getMessage()));
            return false;
        }


    }

    private function overrideEnv()
    {
        try {
            $data = Settings::getSettings('twilio-account-sid');
            if ($data["status"] == 200) {
                $id = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('twilio-auth-token');
            if ($data["status"] == 200) {
                $token = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('twilio-number');
            if ($data["status"] == 200) {
                $number = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('mail-api-key');
            if ($data["status"] == 200) {
                $mail_api_key = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-server-key');
            if ($data["status"] == 200) {
                $server_key = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-sender-id');
            if ($data["status"] == 200) {
                $sender_id = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-server-key-two');
            if ($data["status"] == 200) {
                $server_key2 = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-sender-id-two');
            if ($data["status"] == 200) {
                $sender_id2 = $data['message']['data']['value'];
            }

            $data = Settings::getSettings('mail-provider');
            if ($data["status"] == 200) {
                $mail_provider = $data['message']['data']['value'];
            }

            $data = Settings::getSettings('postal-url');
            if ($data["status"] == 200) {
                $postal_url = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('postal-secret-key');
            if ($data["status"] == 200) {
                $postal_secret_key = $data['message']['data']['value'];
            }

            $data = [
                'TWILIO_ACCOUNT_SID' => $id,
                'TWILIO_AUTH_TOKEN' => $token,
                'TWILIO_NUMBER' => $number,
                'MAIL_API_KEY' => $mail_api_key,
                'FCM_SENDER_ID' => $sender_id,
                'FCM_SERVER_KEY' => $server_key,
                'FCM_SENDER_ID1' => $sender_id2,
                'FCM_SERVER_KEY1' => $server_key2,
                'MAIL_PROVIDER' => $mail_provider,
                'POSTAL_URL' => $postal_url,
                'POSTAL_KEY' => $postal_secret_key

            ];
            if (count($data) > 0) {

                // Read .env-file
                $env = file_get_contents(base_path() . '/.env');

                // Split string on every " " and write into array
                $env = preg_split('/\s+/', $env);;

                Log::info('env into array',[
                    'data' => $env
                ]);

                Log::info('data from setting ',[
                    'data' => $data
                ]);

                // Loop through given data
                foreach ((array)$data as $key => $value) {

                    // Loop through .env-data
                    foreach ($env as $env_key => $env_value) {

                        // Turn the value into an array and stop after the first split
                        // So it's not possible to split e.g. the App-Key by accident
                        $entry = explode("=", $env_value, 2);

                        // Check, if new key fits the actual .env-key
                        if ($entry[0] == $key) {
                            // If yes, overwrite it with the new one
                            $env[$env_key] = $key . "=" . $value;
                        } else {
                            // If not, keep the old one
                            $env[$env_key] = $env_value;
                        }
                    }
                }

                // Turn the array back to an String
                $env = implode("\n", $env);

                // And overwrite the .env with the new data
                file_put_contents(base_path() . '/.env', $env);

                return true;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            Log::info(json_encode($ex->getMessage()));
            return false;
        }

    }

}
