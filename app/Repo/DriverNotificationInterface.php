<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 11/2/17
 * Time: 12:58 PM
 */

namespace App\Repo;


interface DriverNotificationInterface
{
    public function updateNotification($id, $userId, array $attributes);

    public function getNotification($userId,$limit);

//    public function updateAdminNotification($id, $userId, array $attributes);

//    public function getAdminNotification($userId);

    public function getSpecificNotification($id, $userId);

    public function getUnseenNotification($userId);
}