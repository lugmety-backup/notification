<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:22 PM
 */

namespace App\Repo;


interface AdminNotificationStatusInterface
{
    public function getSpecificUserNotificationList($userId);

    public function getAllUserNotificationSeenList($userId,$limit);

    public function getSpecificUserNotificationListByNotificationId($userId, $notificationId);

    public function createAdminNotificationStatus(array $request);

}