<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 1:20 PM
 */

namespace App\Repo;


interface BaseInterface
{
    public function getAll($sortBy,$limit);

    public function create(array $data);

    public function update($id, array $data);

    public function delete($id);

    public function getSpecificById($id);

    public function getAllWithParam(array $parameter, $path);

}