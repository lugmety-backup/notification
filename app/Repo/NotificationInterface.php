<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/3/17
 * Time: 12:29 PM
 */
namespace App\Repo;
interface NotificationInterface
{
    public function updateUserNotification($userId, array $attributes);

    public function getUserNotification($userId,$limit);

//    public function updateAdminNotification($id, $userId, array $attributes);

//    public function getAdminNotification($userId);

    public function getSpecificUserNotification($id, $userId);

    public function getUnseenNotification($userId);
}