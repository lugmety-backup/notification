<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 1:07 PM
 */

namespace App\Repo\Eloquent;

use App\Models\Event;
use App\Repo\EventInterface;


class EventRepo extends BaseRepo implements EventInterface
{
    private $event;
    public function __construct(Event $event)
    {
        parent::__construct($event);
        $this->event =  $event;
    }


}