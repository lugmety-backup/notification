<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/3/17
 * Time: 12:30 PM
 */
namespace App\Repo\Eloquent;
use App\Models\DriverNotification;
use App\Repo\DriverNotificationInterface;


/**
 * Class NotificationRepo
 * @package App\Repo\Eloquent
 */
class DriverNotificationRepo implements DriverNotificationInterface
{
    /**
     * @var notification
     */

    private $notification;

    /**
     * NotificationRepo constructor.
     * @param Notification $notification
     */
    public function __construct(DriverNotification $driverNotification)
    {
        $this->driverNotification = $driverNotification;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function updateNotification($id, $userId, array $attributes)
    {
        return $this->driverNotification->where('user_id', $userId)->whereIn('id',$id)->update($attributes);
    }

    /**
     * @param $id
     * @param $userId
     * @param array $attributes
     * @return mixed
     */
//    public function updateAdminNotification($id, $userId, array $attributes)
//    {
//
//        $this->adminNotification->where('user_id', $userId)->findOrFail($id)->update($attributes);
//        $result = $this->adminNotification->findOrFail($id);
//        return $result;
//    }

    public function getNotification($userId,$limit)
    {
        return $this->driverNotification->where('user_id', $userId)->orderBy('created_at','desc')->paginate($limit);
    }

//    public function getAdminNotification($userId)
//    {
//        return $this->adminNotification->where('user_id', $userId)->get();
//    }

    public function getSpecificNotification($id, $userId)
    {
        return $this->driverNotification->where('user_id', $userId)->findOrFail($id);
    }

    public function getUnseenNotification($userId)
    {
        return $this->driverNotification->where([
            ['user_id',$userId],
            ['status','0']
        ])->count();
    }


}

