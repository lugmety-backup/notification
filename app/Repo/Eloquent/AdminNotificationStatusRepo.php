<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:28 PM
 */

namespace App\Repo\Eloquent;


use App\Models\AdminNotification;
use App\Models\AdminNotificationStatus;
use App\Repo\AdminNotificationStatusInterface;

class AdminNotificationStatusRepo implements AdminNotificationStatusInterface
{
    protected $adminNotificationStatus;

    /**
     * AdminNotificationStatusRepo constructor.
     * @param $adminNotificationStatus
     */
    public function __construct(AdminNotificationStatus $adminNotificationStatus)
    {
        $this->adminNotificationStatus = $adminNotificationStatus;
    }

    public function getSpecificUserNotificationList($userId)
    {
        return $this->adminNotificationStatus->where("user_id",$userId)->get();
    }

    public function createAdminNotificationStatus(array $request)
    {
        return $this->adminNotificationStatus->create($request);
    }

    public function getSpecificUserNotificationListByNotificationId($userId, $notificationId)
    {
      return $this->adminNotificationStatus->where([
          ["user_id" , $userId],
          ["notification_id", $notificationId]
      ])->first();
    }

    public function getAllUserNotificationSeenList($userId,$limit)
    {
        return $this->adminNotificationStatus->where("user_id" , $userId)->paginate($limit);
    }


}