<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:27 PM
 */

namespace App\Repo\Eloquent;


use App\Models\AdminNotification;
use App\Repo\AdminNotificationInterface;

class AdminNotificationRepo implements AdminNotificationInterface
{
    protected $adminNotification;

    /**
     * AdminNotificationRepo constructor.
     * @param $adminNotificatio
     */
    public function __construct(AdminNotification $adminNotification)
    {
        $this->adminNotification = $adminNotification;
    }


    public function getAllNotification($limit)
    {
        return $this->adminNotification->paginate($limit);
    }

    public function getSpecificNotification($id)
    {
       return $this->adminNotification->findOrFail($id);
    }


}