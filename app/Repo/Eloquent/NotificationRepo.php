<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/3/17
 * Time: 12:30 PM
 */
namespace App\Repo\Eloquent;
use App\Models\UserNotification;
use App\Models\AdminNotification;
use App\Repo\NotificationInterface;

/**
 * Class NotificationRepo
 * @package App\Repo\Eloquent
 */
class NotificationRepo implements NotificationInterface
{
    /**
     * @var Notification
     */
    private $adminNotification;


//    private $userNotification;

    /**
     * NotificationRepo constructor.
     * @param Notification $notification
     */
    public function __construct(UserNotification $userNotification)
    {
        $this->userNotification = $userNotification;
    }

    /**
     * @param array $attributes
     * @return mixed
     */
    public function updateUserNotification($userId, array $attributes)
    {
        return $this->userNotification->where([
            ['user_id',$userId],
            ['status','0']])->update($attributes);

    }

    /**
     * @param $id
     * @param $userId
     * @param array $attributes
     * @return mixed
     */
//    public function updateAdminNotification($id, $userId, array $attributes)
//    {
//
//        $this->adminNotification->where('user_id', $userId)->findOrFail($id)->update($attributes);
//        $result = $this->adminNotification->findOrFail($id);
//        return $result;
//    }

    public function getUserNotification($userId,$limit)
    {
        return $this->userNotification->where('user_id', $userId)->orderBy('created_at','desc')->paginate($limit);
    }

//    public function getAdminNotification($userId)
//    {
//        return $this->adminNotification->where('user_id', $userId)->get();
//    }

    public function getSpecificUserNotification($id, $userId)
    {
        return $this->userNotification->where('user_id', $userId)->findOrFail($id);
    }

    public function getUnseenNotification($userId)
    {
        return $this->userNotification->where([
            ['user_id',$userId],
            ['status','0']
        ])->count();
    }


}

