<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 2:42 PM
 */

namespace App\Repo\Eloquent;

use App\Models\CustomNotification;
use App\Repo\CustomNotificationInterface;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;


class CustomNotificationRepo extends BaseRepo implements CustomNotificationInterface
{
    private $notification;

    public function __construct(CustomNotification $notification)
    {
        parent::__construct($notification);
        $this->notification = $notification;
    }

    public function getAllWithParam(array $parameter, $path)
    {
        $columnsList = Schema::getColumnListing('custom_notifications');

        //$is_columnExistInUserTable = false;

        $orderByColumn = "id";
        foreach ($columnsList as $columnName) {
            if ($columnName == $parameter["sort_field"]) {
                $orderByColumn = $columnName;
                break;
            }
        }
        $parameter["sort_field"] = $orderByColumn;
        if (isset($parameter["filter_field"])) {
            if (in_array($parameter["filter_field"], $columnsList)) {
                $data = $this->notification->where($parameter["filter_field"], $parameter["filter_value"]);
            } else {
                $data = $this->notification;
            }


        } else {
            $data = $this->model;
        }
        if (isset($parameter["q"])) {
            $searchValue = "%" . $parameter["q"] . "%";

            $data = $data->where(function ($query) use ($searchValue, $columnsList) {
                foreach ($columnsList as $key => $columnName) {

                    $query->orWhere($columnName, "like", $searchValue);


                }

            });


        }
        return $data->with(['event:id,title'])->orderBy($orderByColumn, $parameter["sort_by"])->paginate($parameter["limit"])->withPath($path)->appends($parameter);
    }

    public function getSpecificById($id)
    {
        return $this->notification->with(['event'])->findOrFail($id);

    }

    public function getAllWithParamWithTimeZone($parameter, $path, $timezone)
    {
        $columnsList = Schema::getColumnListing('custom_notifications');

        //$is_columnExistInUserTable = false;

        $orderByColumn = "id";
        foreach ($columnsList as $columnName) {
            if ($columnName == $parameter["sort_field"]) {
                $orderByColumn = $columnName;
                break;
            }
        }
        $parameter["sort_field"] = $orderByColumn;
        if (isset($parameter["filter_field"])) {
            if (in_array($parameter["filter_field"], $columnsList)) {
                $data = $this->notification->where($parameter["filter_field"], $parameter["filter_value"]);
            } else {
                $data = $this->notification;
            }


        } else {
            $data = $this->model;
        }
        if (isset($parameter["q"])) {
            $searchValue = "%" . $parameter["q"] . "%";

            $data = $data->where(function ($query) use ($searchValue, $columnsList) {
                foreach ($columnsList as $key => $columnName) {

                    $query->orWhere($columnName, "like", $searchValue);


                }

            });


        }
        return $data->with(['event:id,title'])->select(DB::raw('*,CONVERT_TZ (created_at, "+00:00","' . $timezone . '") as created_at'))->orderBy($orderByColumn, $parameter["sort_by"])->paginate($parameter["limit"])->withPath($path)->appends($parameter);
    }


}