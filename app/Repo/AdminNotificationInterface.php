<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:21 PM
 */

namespace App\Repo;


interface AdminNotificationInterface
{
    public function getAllNotification($limit);

    public function getSpecificNotification($id);

}