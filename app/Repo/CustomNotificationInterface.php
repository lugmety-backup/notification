<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 2:43 PM
 */

namespace App\Repo;


interface CustomNotificationInterface
{
    public function getAllWithParam(array $parameter, $path);

    public function getSpecificById($id);

    public function getAllWithParamWithTimeZone($parameter, $path, $timezone);

}