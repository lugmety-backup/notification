<?php

namespace App\Providers;

use App\Events\Event;
use App\Repo\AdminNotificationInterface;
use App\Repo\AdminNotificationStatusInterface;
use App\Repo\CustomNotificationInterface;
use App\Repo\DriverNotificationInterface;
use App\Repo\Eloquent\AdminNotificationRepo;
use App\Repo\Eloquent\AdminNotificationStatusRepo;
use App\Repo\Eloquent\CustomNotificationRepo;
use App\Repo\Eloquent\DriverNotificationRepo;
use App\Repo\Eloquent\EventRepo;
use App\Repo\Eloquent\NotificationRepo;
use App\Repo\EventInterface;
use App\Repo\NotificationInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(NotificationInterface::class, NotificationRepo::class);
        $this->app->bind(AdminNotificationInterface::class,AdminNotificationRepo::class);
        $this->app->bind(AdminNotificationStatusInterface::class,AdminNotificationStatusRepo::class);
        $this->app->bind(DriverNotificationInterface::class, DriverNotificationRepo::class);
        $this->app->bind(EventInterface::class, EventRepo::class);
        $this->app->bind(CustomNotificationInterface::class, CustomNotificationRepo::class);

        $this->app->bind('remoteCall','App\Http\RemoteCallFacade');
        $this->app->bind('roleChecker', 'App\Http\RoleCheckerFacade');
        $this->app->bind('userDetail', 'App\Http\UserDetailFacade');
        $this->app->bind('settings','App\Http\SettingsFacade');
    }

    public function boot(){
        /**
         * takes the google cloud credential
         */
        $data = file_get_contents(storage_path() . '/google-cloud-pvt-key.txt');
        $array = [
            "type" =>env('STACK_TYPE') ,
            "project_id" => env('STACK_PROJECT_ID'),
            "private_key_id" => env('STACK_PRIVATE_KEY_ID'),
            "private_key" => str_replace('\n', "\n", $data),
            "client_email" => env('STACK_CLIENT_EMAIL'),
            "client_id" => env('STACK_CLIENT_ID'),
            "auth_uri" => env('STACK_AUTH_URI'),
            "token_uri" => env('STACK_TOKEN_URL'),
            "auth_provider_x509_cert_url" => env('STACK_AUTH_PROVIDER_x509_CERT_URL'),
            "client_x509_cert_url" => env('STACK_CLIENT_X509_CERT_URL')
        ];
        /**
         * the path to create and store the json
         */
        $path = storage_path().'/stackdriver.json';
        /**
         * open the file
         */
        $fp = fopen($path, 'w');
        /**
         * store array in json with pretty print
         */
        fwrite($fp, json_encode($array,JSON_PRETTY_PRINT ));
        fclose($fp);
    }
}
