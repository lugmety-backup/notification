<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 9/3/17
 * Time: 11:14 AM
 */

namespace App\Http;

use RemoteCall;

use Illuminate\Support\Facades\Config;

class UserDetailFacade
{
    public function getUserDetails($userId, $restaurantId, $driverId, $parentId, $countryId)
    {
        $details = RemoteCall::getSpecificWithoutLogin(Config::get("config.oauth_base_url") . "/notification/info?user_id=" . $userId . "&restaurant_id=" . $restaurantId . "&driver_id=" . $driverId. "&parent_id=".$parentId."&country_id=".$countryId);

        if (isset($details['message']['status']) && $details['message']['status'] != 200) {

            return $details['message'];

        }
        if (!empty($details['message']['data']['restaurant_admin'])) {

                $adminEmail[] = $details['message']['data']['restaurant_admin']['email'];
                $adminMobile[] = $details['message']['data']['restaurant_admin']['mobile'];

        }
        $operatorTokens = [];

        if(count($details['message']['data']['branch_operator']) > 0)
        {
            foreach ($details['message']['data']['branch_operator'] as $operator) {
                if(count($operator['tokens']) > 0 )
                {

                    $operatorTokens[] = $operator['tokens'];

                }
            }

        }

        if (!empty($details['message']['data']['branch_admin'])) {

                $adminEmail[] = $details['message']['data']['branch_admin']['email'];
                $adminMobile[] = $details['message']['data']['branch_admin']['mobile'];

        }

        foreach ($details['message']['data']['admin'] as $admin) {
            $adminEmail[] = $admin['email'];
            $adminMobile[] = $admin['mobile'];
        }

        $details['message']['data']['adminEmailList'] = $adminEmail;
        $details['message']['data']['adminMobileList'] = $adminMobile;
        $details['message']['data']['operatorTokens'] = array_collapse($operatorTokens);

        return $details['message'];
        /**
         * Check if $restaurant["message"]->message[0]->status is available and active. If active return true, else false.
         */


    }
    public function getResAdminDetails($userId, $restaurantId, $driverId, $parentId)
    {
        $details = RemoteCall::getSpecificWithoutLogin(Config::get("config.oauth_base_url") . "/notification/info?user_id=" . $userId . "&restaurant_id=" . $restaurantId . "&driver_id=" . $driverId. "&parent_id=".$parentId);

        if (isset($details['message']['status']) && $details['message']['status'] != 200) {

            return $details['message'];

        }
        if (!empty($details['message']['data']['restaurant_admin'])) {

            $adminEmail[] = $details['message']['data']['restaurant_admin']['email'];
            $adminMobile[] = $details['message']['data']['restaurant_admin']['mobile'];

        }

        if (!empty($details['message']['data']['branch_admin'])) {

            $adminEmail[] = $details['message']['data']['branch_admin']['email'];
            $adminMobile[] = $details['message']['data']['branch_admin']['mobile'];

        }

        $details['message']['data']['adminEmailList'] = $adminEmail;
        $details['message']['data']['adminMobileList'] = $adminMobile;

        return $details['message'];
        /**
         * Check if $restaurant["message"]->message[0]->status is available and active. If active return true, else false.
         */


    }

}