<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 9/3/17
 * Time: 11:13 AM
 */

namespace App\Http\Facades;

use Illuminate\Support\Facades\Facade;
class UserDetailFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return "userDetail";
    }
}