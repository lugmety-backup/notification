<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 2:38 PM
 */

namespace App\Http\Controllers;


use App\Events\SendNotification;
use App\Repo\CustomNotificationInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RoleChecker;
use RemoteCall;

class CustomNotificationController extends Controller
{
    private $notification;

    public function __construct(CustomNotificationInterface $notification)
    {
        $this->notification = $notification;

    }

    public function index(Request $request)
    {

        $userId = RoleChecker::getUser();

        $details = RemoteCall::getSpecificWithoutLogin(Config::get("config.oauth_base_url") . "/user/notification-info/$userId");

        if ($details['status'] != 200) {
            return response()->json([
                $details['message']
            ], $details['status']);

        }

        $timeZone = $details['message']['data']['timezone'];

        $timeZoneInISO = Carbon::now($timeZone)->toIso8601String();

        $timeZOneInArray = explode("+", $timeZoneInISO);

        if (isset($timeZOneInArray[1])) {
            $timeZoneInUtc = "+" . $timeZOneInArray[1];
        } else {
            $timeZOneInArray = explode("-", $timeZone);
            $timeZoneInUtc = "-" . $timeZOneInArray[1];
        }


        try {
            $sortBy = $request->get("sort_by", "desc");
            $sortField = $request->get("sort_field");
            $limit = $request->get("limit", 10);
            try {
                $this->validate($request, [
                    "filter_field" => "sometimes|string",
                    "filter_value" => "required_with:filter_field|string",
                    "q" => "sometimes",
                ]);
            } catch (\Exception $ex) {
                Log::error("Event List Display", [
                    "status" => "422",
                    "message" => serialize($ex->response->original),
                    "request" => serialize($request->all())
                ]);

                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }

            try {
                $this->validate($request, [
                    "limit" => "required|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                $limit = 10;
            }

            $parameter = $request->all();
            $parameter["sort_by"] = $sortBy;
            $parameter["sort_field"] = $sortField;
            $parameter["limit"] = $limit;
            $path = '/admin/custom-notification';

            $data = $this->notification->getAllWithParamWithTimeZone($parameter, $path, $timeZoneInUtc);

            if (count($data) == 0) {
                Log::info("Notification List Display", [
                    "status" => "204",
                    "message" => "No record found",
                    "request" => serialize($request->all())
                ]);
                return response()->json([
                    "status" => "204",
                    "message" => "No record found"
                ], 204);
            }
            Log::info("Notification List Display", [
                "status" => "200",
                "data" => serialize($data),
                "request" => serialize($request->all())
            ]);
            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        } catch (\Exception $ex) {
            Log::error("Notification List Display", [
                'status' => "500",
                'message' => $ex->getMessage(),
                "request" => serialize($request->all())
            ]);


            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }


    }

    /**
     *
     * Store notification
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function store(Request $request)
    {
        $messages = [
            "array" => "The meta data must be json object."
        ];

        try {
            $this->validate($request, [
                "title" => "required",
                "event_id" => "required|exists:events,id",
                "event_type" => "required",
                "user_role" => "required|in:customer,driver",
                "meta_data" => "required_if:event_type,new-restaurant,array",
                "meta_data.branch_id" => "required_if:event_type,new-restaurant,integer",
                "content" => "required",
                "country_id" => "required|integer",
                "targeted_user" => "required|in:logged-in-users,all"
            ], $messages);
        } catch (\Exception $ex) {

            Log::error("Notification Validation", [
                "status" => "422",
                "message" => serialize($ex->response->original),
                "request" => serialize($request->all())
            ]);
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        /**
         * Start DB transaction to ensure the operation is reversed in case not successfully committed.
         *
         */

        try {

            DB::beginTransaction();

            $request['meta_data'] = serialize($request['meta_data']);
            $notification = $this->notification->create($request->only('title', 'content', 'meta_data', 'user_role', 'event_id', 'country_id','targeted_user'));


            DB::commit();
            Log::info("notification Create", [
                "status" => "200",
                "message" => "Custom notification Created Successfully",
                "request" => serialize($request->all())
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Custom notification Created Successfully"
            ], 200);

        } catch (\Exception $ex) {
            Log::error("notification Create", [
                'status' => "500",
                'message' => $ex->getMessage(),
                "request" => serialize($request->all())
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);
        }

    }

    public function adminShow($id)
    {
        try {

            $data = $this->notification->getSpecificById($id);

            $data['meta_data'] = unserialize($data['meta_data']);

            Log::info("notification Display", [
                "status" => "200",
                "data" => serialize($data),
                "id" => $id
            ]);

            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("custom notification Display", [
                "status" => "204",
                "message" => "No record found",
                "id" => $id
            ]);
            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);

        } catch (\Exception $ex) {
            Log::error("custom notification Display", [
                'status' => "500",
                'message' => $ex->getMessage(),
                'id' => $id
            ]);

            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }

    }

    public function update($id, Request $request)
    {


        /**
         * Start DB transaction to ensure the operation is reversed in case not successfully committed.
         *
         */
        try {
            $notification = $this->notification->getSpecificById($id);
            try {
                $messages = [
                    "array" => "The meta data must be json object"
                ];
                $this->validate($request, [
                    "title" => "required",
                    "event_id" => "required|exists:events,id",
                    "event_type" => "required",
                    "user_role" => "required|in:customer,driver",
                    "meta_data" => "required_if:event_type,new-restaurant,array",
                    "meta_data.branch_id" => "required_if:event_type,new-restaurant,integer",
                    "content" => "required",
                    'country_id' => "required|integer",
                    "targeted_user" => "required|in:logged-in-users,all"
                ], $messages);
            } catch (\Exception $ex) {

                Log::error("Notification Validation", [
                    "status" => "422",
                    "message" => serialize($ex->response->original),
                    "request" => serialize($request->all())
                ]);
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }

            DB::beginTransaction();

            $request['meta_data'] = serialize($request['meta_data']);

            $this->notification->update($id, $request->only('title', 'content', 'meta_data', 'user_role', 'event_id','targeted_user'));

            DB::commit();

            Log::info("custom notification Update", [
                'status' => "204",
                'message' => "Custom notification Updated Successfully",
                'request' => $request->all(),
                'id' => $id
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Custom notification Updated Successfully"
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("custom notification Update", [
                'status' => "204",
                'message' => "No record found",
                'id' => $id
            ]);

            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);
        } catch (\Exception $ex) {
            Log::error("custom notification Update", [
                'status' => "500",
                'user_id' => $ex->getMessage(),
                'id' => $id
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);

        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $notification = $this->notification->getSpecificById($id);

            $this->notification->delete($id);

            DB::commit();

            Log::info("custom notification Delete", [
                'status' => "200",
                'message' => "Custom notification successfully deleted.",
                'id' => $id
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Custom notification successfully deleted."
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("custom notification Delete", [
                'status' => "204",
                'message' => "No record found",
                'id' => $id
            ]);

            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);

        } catch (\Exception $ex) {
            Log::error("custom notification Delete", [
                'status' => "500",
                'message' => $ex->getMessage(),
                'id' => $id
            ]);

            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }

    }

    public function trigger($id)
    {
        try {

            $data = $this->notification->getSpecificById($id);
            $data['meta_data'] = unserialize($data['meta_data']);

            if ($data['sent'] == "1") {
                Log::info("notification trigger", [
                    "status" => "409",
                    "message" => "Notification has been already triggered.",
                    "data" => serialize($data),
                    "id" => $id
                ]);

                return response()->json([
                    "status" => "409",
                    "message" => "Notification has been already triggered.",
                ], 409);
            }

            $data = $data->toArray();
            $data["action"] = $data['event']['slug'];
            $data['notification_type'] = "push-notification";
            $data['service'] = 'custom-notification';

            $request['sent'] = "1";

            event(new SendNotification($data));

            $this->notification->update($id, $request);

            Log::info("notification Display", [
                "status" => "200",
                "data" => serialize($data),
                "id" => $id
            ]);

            return response()->json([
                "status" => "200",
                "message" => "The notification has been triggered successfully."
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("custom notification Display", [
                "status" => "204",
                "message" => "No record found",
                "id" => $id
            ]);
            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);

        } catch (\Exception $ex) {
            Log::error("custom notification Display", [
                'status' => "500",
                'message' => $ex->getMessage(),
                'id' => $id
            ]);

            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }

    }


}