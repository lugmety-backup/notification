<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Log;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotification;
use LaravelFCM\Message\PayloadNotificationBuilder;
use LaravelFCM\Message\Topics;
use Settings;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;
class ExampleController extends Controller
{


    public function testnotification($token){


        $sub = 'New Order';
            $msg = array
            (
                'content' 	=> "Hello Bijay",
                'title'		=> $sub,
                'order_id'	=>  10,
                'type' => 'new',
                'tickerText'	=> '',
                'vibrate'	=> 1,
                'sound'		=> 1,
                'largeIcon'	=> 'large_icon',
                'smallIcon'	=> 'small_icon'
            );
            $fields = array
            (
                'registration_ids' 	=> array($token),
                "priority" => "high",
                "content_available" => true,
                'data'			=> $msg
            );

            $headers = array
            (
                'Authorization: key='. Config::get('fcm.http.server_key1'),
                'Content-Type: application/json'
            );

             Log::info(serialize( array('type' => 'before_new_notification', 'data' => $fields, 'headers' => $headers )));

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );

            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch );

            Log::info(serialize($result));
            curl_close( $ch );
            return true;
    }

    public  function sendMessageWithOldStyleWithTopic(){
        $notificationBuilder = new PayloadNotificationBuilder('my title');
        $notificationBuilder->setBody('Hello world')
            ->setSound('default');

        $notification = $notificationBuilder->build();

        $topic = new Topics();
        $topic->topic('news');

        $topicResponse = \FCM::sendToTopic($topic, null, $notification, null);
//        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
//        dd($downstreamResponse);
        Log::info("old_fcm_v1_custom_notification_topic",["data" => $notification,"response" => $topicResponse ]);

        return true;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function sendMessage($template, $user, $sender)
    {
        try {
            $apiKey = "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";
            $value = view($template)->with(["user" => $user]);
            $value = (string)$value;
            $title = $user['title1'];

            foreach ($user["email"] as $email) {
                Log::info($email);
                $request_body = array(
                    "personalizations" => array(array(
                        "to" => array(
                            array("email" => $email)
                        ),
                        "subject" => $title
                    )),
                    "from" => array("name" => "test", "email" => "no-reply@lugmety.com"),
                    "content" => array(array("type" => "text/html", "value" => $value))
                );


                $sg = new \SendGrid($apiKey);

                $response = $sg->client->mail()->send()->post($request_body);
                $statusCode = $response->statusCode();
                $flag= true;
                Log::info("$statusCode");
                if (!$flag) {
//                    $log =  json_encode($response->headers());
                    throw new \Exception();
                }
            }


            return true;
        }

        catch (\Exception $ex){
            Log::error($ex->getMessage());
            return false;
        }
//        Mail::send($template, ["user" => $user], function ($message) use ($user, $sender) {
//
//            $message->subject($user['title1']);
//            $message->from($sender, $this->senderName);
//
//            $message->to($user['email']);
//
//        });
//        return true;
    }

    public function test(){
        $user["name"]= "ashish";
        $user["url"] = "www.facebook.com";
        $user["email"] = ['ascc.2023@gmail.com',"luitelbj@gmail.com"];
        $template = "oauthService.forgotPassword";
        $user['title1'] = 'Lugmety: Your Lugmety Password Reset';
        if ($this->sendMessage($template, $user, "avskfle@gmail.com") == true){
            Log::info("sucess");

        };
        /**
         * admin part
         */
//        $details = \UserDetail::getUserDetails(null, null, null, null);
//
////
//        if ($details['status'] != 200) {
//            return false;
//        }
//        $user["email"] = $details['data']['adminEmailList'];
//        Log::info(serialize($user["email"]));
//        $user["id"] =1;
//        $user['title1'] = 'Lugmety: Lugmety Password Reset';
//        $adminTemplate = "oauthService.admin.forgotPassword";
//        $this->sendMessage($adminTemplate, $user, "avskfle@gmail.com");

    }

    public function testFsm($token){
        try {
            $fcm['driver'] = 'http';
            $fcm['log_enabled'] = false;
            $fcm['http']['server_send_url'] = 'https://fcm.googleapis.com/fcm/send';
            $fcm['http']['server_group_url'] = 'https://android.googleapis.com/gcm/notification';
            $fcm['http']['timeout'] = 30.0;
            $data = Settings::getSettings('fcm-server-key');
            if ($data["status"] == 200) {
                $fcm['http']['server_key'] = $data['message']['data']['value'];
            }
            $data = Settings::getSettings('fcm-sender-id');
            if ($data["status"] == 200) {
                $fcm['http']['sender_id'] = $data['message']['data']['value'];
            }

//

            $data = var_export($fcm, 1);
            if (File::put(base_path() . '/config/fcm.php', "<?php\n return $data ;")) {
//                Log::info('fcm send data',[serialize($pushData)]);
                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60 * 20);

                $sub = ["Order Picked"];
                $notificationBuilder = new PayloadNotificationBuilder($sub);
                $notificationBuilder->setBody("order testing")
                    ->setTag("1")
                    ->setTitle("testing")
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();

                $dataBuilder->addData([
                    'order_id' => "1",
                    'type' => $sub,
                ]);

                $option = $optionBuilder->build();
                $notification = $notificationBuilder->build();
                $data = $dataBuilder->build();
                $downstreamResponse = \FCM::sendTo($token, $option, $notification, $data);

                Log::info("fcm response",[serialize($downstreamResponse)]);

                return true;

            }
        } catch (\Exception $ex) {
            Log::info(json_encode($ex->getMessage()));
            return $ex->getMessage();
        }

    }

    //
}
