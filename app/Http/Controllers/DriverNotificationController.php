<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/3/17
 * Time: 11:59 AM
 */

namespace App\Http\Controllers;

use App\Repo\DriverNotificationInterface;

use Carbon\Carbon;
use Illuminate\Http\Request;
use RoleChecker;

class DriverNotificationController extends Controller
{
    /**
     * @var DriverNotificationInterface
     */
    private $notification;

    /**
     * DriverNotificationController constructor.
     * @param DriverNotificationInterface $notification
     */

    public function __construct(DriverNotificationInterface $notification)
    {
        $this->notification = $notification;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllUserNotifications(Request $request)
    {

       $limit =  (integer)$request->get("limit", 10);

        try {
            $this->validate($request, [
                "limit" => "required|integer|min:1"
            ]);
        } catch (\Exception $ex) {
            $limit = 10;
        }

        if (!(RoleChecker::hasRole("driver") || RoleChecker::hasRole("driver-captain"))) {
            return response()->json([
                "status" => "403",
                "message" => "Only driver can view notification history from here",
            ], 403);
        }

            $userId = RoleChecker::getUser();

        try {
            $notifications = $this->notification->getNotification($userId, $limit);

            if (!$notifications->first()) {
                throw  new \Exception();
            }
            $logo = "http://api.stagingapp.io/cdn/v1/images/34716633959d9fecedae91.png";
            foreach ($notifications as $notification) {

                $notification['status'] = "1";
                $notID[] = $notification['id'];
                $data = unserialize($notification['data']);
                if ($data['service'] == 'order service') {
                    $order = $data['data']['order'];
                    $notification['icon'] = empty($order['restaurant']['logo']) ? $logo : $order['restaurant']['logo'];
                    $notification['type'] = 'order';
                    $notification['has_detail'] = 'true';
                    $notification['action_type'] = $notification['title'];
                    $notification['title'] = $order['restaurant']['name'];
                    $notification['data'] = [
                        'restaurant_id' => isset($order['restaurant']['id'])? $order['restaurant']['id']:'',
                        'order_id' => isset($order['id']) ? $order['id']: 0 ,
                    ];
                } else {


                    $notification['icon'] = $logo;
                    $notification['type'] = 'user';
                    $notification['has_detail'] = 'false';
                    $notification['action_type'] = $notification['title'];
                    $notification['data'] = new \stdClass();
                }
                $date = Carbon::parse($notification['created_at'])->diffForHumans();
                $notification['time'] = $this->changeDate($date);

                $notification = array_except($notification, 'created_at, updated_at', 'user_id');


            }
            $update['status'] = '1';
            $this->notification->updateNotification($notID, $userId, $update);

            return response()->json([
                "status" => "200",
                "data" => $notifications->withPath("/user/notifications"),
            ], 200);
        } catch (\Exception $ex) {
            return response()->json(["status" => "404",
                "message" => "Empty Record"], 404);
        }


    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSpecificUserNotification($id)
    {

        if (!(RoleChecker::hasRole("driver") || RoleChecker::hasRole("driver-captain"))) {
            return response()->json([
                "status" => "403",
                "message" => "Only driver can view notification from here",
            ], 403);
        }
        try {
            $userId = RoleChecker::getUser();
            $update['status'] = '1';
            $notify = $this->notification->getSpecificNotification($id, $userId);
            $logo = "http://api.stagingapp.io/cdn/v1/images/34716633959d9fecedae91.png";
            $data = unserialize($notify['data']);
            $notification['action_type'] = $notify['title'];
            if ($data['service'] == 'order service') {
                $order = $data['data']['order'];
                $notification['icon'] = empty($order['restaurant']['logo']) ? $logo : $order['restaurant']['logo'];
                $notification['type'] = 'order';
                $notification['has_detail'] = 'true';
                $notification['title'] = $order['restaurant']['name'];
                $notification['description'] = $notify['description'];
                $notification['data'] = [
                    'restaurant_id' => $order['restaurant']['id'],
                    'order_id' => $order['id']
                ];
            } else {


                $notification['icon'] = $logo;
                $notification['type'] = 'user';
                $notification['has_detail'] = 'false';
                $notification['title'] = $notify['title'];
                $notification['data'] = new \stdClass();
            }

            $notification['created_at'] = $this->changeDate(Carbon::parse($notify['created_at'])->diffForHumans());
            $updateNotification = $notify->update($update);

            if (!$notify->first()) {
                throw  new \Exception();
            }


            return response()->json([
                "status" => "200",
                "data" => $notification,
            ], 200);
        } catch (\Exception $ex) {
            return response()->json(["status" => "404",
                "message" => "Empty Record"], 404);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserNotification($id)
    {
        if (!(RoleChecker::hasRole("driver") || RoleChecker::hasRole("driver-captain"))) {
            return response()->json([
                "status" => "403",
                "message" => "Only customer can view notification from here",
            ], 403);
        }
        try {
            $userId = RoleChecker::getUser();

            $update['status'] = '1';
            $notification = $this->notification->updateNotification($id, $userId, $update);

            if (!$notification->first()) {
                throw  new \Exception();
            }
            $notification['data'] = unserialize($notification['data']);

            return response()->json([
                "status" => "200",
                "data" => $notification,
            ], 200);
        } catch (\Exception $ex) {
            return response()->json(["status" => "404",
                "message" => "Empty Record"],
                404);
        }
    }

    private function changeDate($date)
    {
        $data = explode(" ", $date);
        if (strpos($data[1], 'second') !== false) {
            return str_replace(['seconds', 'second'], 's', $date);
        }
        if (strpos($data[1], 'minute') !== false) {
            return str_replace([' minutes', ' minute'], 'm', $date);
        }
        if (strpos($data[1], 'hour') !== false) {
            return str_replace([' hours', ' hour'], 'h', $date);
        }
        if (strpos($data[1], 'day') !== false) {
            return str_replace([' days', ' day'], 'd', $date);
        }
        if (strpos($data[1], 'week') !== false) {
            return str_replace([' weeks', ' week'], 'w', $date);
        }
        if (strpos($data[1], 'month') !== false) {
            return str_replace([' months', ' month'], 'M', $date);
        }
        if (strpos($data[1], 'year') !== false) {
            return str_replace([' years', ' year'], 'y', $date);
        }

    }

    public function getUnseenNotification()
    {
        $userId = RoleChecker::getUser();
        $count = $this->notification->getUnseenNotification($userId);
        return response()->json([
            "status" => "200",
            "data" => $count,
        ], 200);
    }

}