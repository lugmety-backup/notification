<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/28/17
 * Time: 4:00 PM
 */

namespace App\Http\Controllers\NotificationClass;


interface ProcessInterface
{
    public function sendMessage($tempate, $user, $sender);

}