<?php

namespace App\Http\Controllers\NotificationClass;
use Postal;
use App\Http\Controllers\NotificationClass\ProcessInterface;
use App\Http\Helpers\ExcelExportHelper;
use App\Models\AdminNotification;
use App\Models\DriverNotification;
use App\Models\UserNotification;
use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Mpdf\Mpdf;
use Prophecy\Exception\Prediction\PredictionException;
use UserDetail;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;
use Settings;
use App\Models\EmailNotification as MailNotification;
use Carbon\Carbon;
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/28/17
 * Time: 3:02 PM
 */
class EmailNotification implements ProcessInterface, SupportedMethodInterface
{

    protected $serviceName;
    protected $email;
    protected $subject;
    protected $userId;
    protected $mainData;
    protected $redisDataAdminList;
    protected $id;
    protected $sender;
    protected $senderName;
    protected $excelHelper;
    protected $replyEmail;
    protected $replyToName;

    /**
     * EmailNotification constructor.
     * @param $emailData
     */
    public function __construct($emailData)
    {
        $this->id = $emailData['id'];
        $this->email = $emailData["email"];
        $this->subject = $emailData["subject"];
        $this->userId = $emailData["user_id"];
        $this->excelHelper = new ExcelExportHelper();
        $this->mainData = unserialize($emailData["data"]);
        $this->serviceName = $this->mainData["service"];
        $this->action = $this->mainData['action'];
        $this->redisDataAdminList = explode(",", \Redis::get("admin-list"));
        $dataSet = Settings::getSettings('sender-email');
        if ($dataSet["status"] == 200) {
            $this->sender = $dataSet['message']['data']['value'];
        }
        $dataSet = Settings::getSettings('sender-name');
        if ($dataSet["status"] == 200) {
            $this->senderName = $dataSet['message']['data']['value'];
        }
        $this->callFunction();


    }

    /**
     * Call functions according to the service name.
     */
    private function callFunction()
    {
        if ($this->serviceName == "order service") $this->orderService();
        elseif ($this->serviceName == "oauth service") $this->oauthService();
        elseif ($this->serviceName == "restaurant service") $this->restaurantService();
        elseif ($this->serviceName == "general service") $this->generalService();

    }

    /**
     * Function to send mail to the users, admin or driver
     * @param $template
     * @param $user
     * @param $sender
     * @return bool
     */
    public function sendMessage($template, $user, $sender)
    {
        $mail_provider = env('MAIL_PROVIDER');

        Log::info('mail_provider',[$mail_provider]);
        if($mail_provider == 'sendgrid'){
            return $this->sendMessageSendGrid($template,$user,$sender);
        }
        elseif ($mail_provider == 'postal'){
            return $this->sendMessageViaPostal($template,$user,$sender);
        }
        else{
            return $this->sendMessageViaPostal($template,$user,$sender);
        }
    }



    private function sendMessageSendGrid($template, $user, $sender)
    {
        try {
            $apiKey = env("MAIL_API_KEY");
            $value = view($template)->with(["user" => $user]);
            $value = (string)$value;
            $title = $user['title1'];
            if(!is_array($user["email"])){
                $user["email"] = array($user["email"]);
            }
            foreach ($user["email"] as $email) {
                $request_body = array(
                    "personalizations" => array(array(
                        "to" => array(
                            array("email" => $email)
                        ),
                        "subject" => $title
                    )),
                    "from" => array("name" => $this->senderName, "email" => $sender),
                    "content" => array(array("type" => "text/html", "value" => $value))
                );


                $sg = new \SendGrid($apiKey);

                $response = $sg->client->mail()->send()->post($request_body);
                $statusCode = $response->statusCode();
                if($statusCode == 200 || $statusCode == 202){
                    $flag = true;

                }
                else{
                    $flag = false;
                }

                Log::info("email_notification_sendgrid", [ 'status_code' => $statusCode, 'content'   => $request_body['personalizations'] ]);

                if (!$flag) {
                    $log = serialize($response->headers());
                    throw new \Exception($log);
                }
            }

            return true;
        }

        catch (\Exception $ex){
            Log::error("error sending mail",[serialize($ex->getMessage())]);
            return false;
        }
//        Mail::send($template, ["user" => $user], function ($message) use ($user, $sender) {
//
//            $message->subject($user['title1']);
//            $message->from($sender, $this->senderName);
//
//            $message->to($user['email']);
//
//        });
//        return true;
    }


    private function sendMessageViaPostal($template, $user, $sender){
        try {
            $postalUrl = env('POSTAL_URL');
            $postal_key = env('POSTAL_KEY');
            $value = view($template)->with(["user" => $user]);
            $value = (string)$value;
            $title = $user['title1'];
            if(!is_array($user["email"])){
                $user["email"] = array($user["email"]);
            }
            foreach ($user["email"] as $email) {
                // Create a new Postal client using the server key you generate in the web interface
                $client = new Postal\Client($postalUrl, $postal_key);

// Create a new message
                $message = new Postal\SendMessage($client);

// Add some recipients
                $message->to($email);
//                $message->to('mary@example.com');
//                $message->cc('mike@example.com');
//                $message->bcc('secret@awesomeapp.com');

// Specify who the message should be from. This must be from a verified domain
// on your mail server.
                $message->from("$this->senderName <".$sender.'>');
                $message->sender("$this->senderName <".$sender.'>');
                $message->replyTo("$this->replyToName <".$this->replyEmail.'>');

// Set the subject
                $message->subject($title);

// Set the content for the e-mail
                $message->htmlBody($value);

// Add any custom headers
//                $message->header('X-PHP-Test', 'value');

// Attach any files
//                $message->attach('textmessage.txt', 'text/plain', 'Hello world!');

// Send the message and get the result
                $result = $message->send();
//                $statusCode = $result->get
                Log::info("email_notification_postal", [ 'status_code' => '200', 'content'   => $result->recipients()]);
//
//                if (!$flag) {
//                    $log = serialize($response->headers());
//                    throw new \Exception($log);
//                }
            }

            return true;
        }

        catch (\Exception $ex){
            Log::error("error sending mail",[serialize($ex->getMessage())]);
            return false;
        }
    }



    private function sendInvoiceEmail(  $template, $user, $sender){
        try {
            $apiKey = "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";
            $value = view($template)->with(["user" => $user]);
            $value = (string)$value;
            $title = $user['title1'];
            if(!is_array($user["email"])){
                $user["email"][] = array($user["email"]);
            }
            $filename = $user["filename"];
            foreach ($user["email"] as $email) {
                $request_body = array(
                    "personalizations" => array(
                        array(
                            "cc" => $user["adminCC"],
                            "to" => array(
                                array("email" => $email),
                            ),

                            "subject" => $title
                        )),
                    "from" => array("name" => $this->senderName, "email" => $sender),
                    "content" => array(array("type" => "text/html", "value" => $value)),
                    'attachments' => [
                        [
                            'content' => base64_encode(file_get_contents(storage_path( "exports/$filename.zip") )),
                            'type' => 'application/zip',
                            'filename' => "$filename.zip",
                            'disposition' => 'attachment'
                        ]
                    ]
                );


                $sg = new \SendGrid($apiKey);

                $response = $sg->client->mail()->send()->post($request_body);
                $statusCode = $response->statusCode();
                if($statusCode == 200 || $statusCode == 202){
                    $flag = true;

                }
                else{
                    $flag = false;
                }
                Log::info("$statusCode");
                if (!$flag) {
                    $log = serialize($response->headers());
                    throw new \Exception($log);
                }
            }

            return true;
        }

        catch (\Exception $ex){
            Log::error("error sending mail",[serialize($ex->getMessage())]);
            return false;
        }
    }


    public function generalService(){
        try{
            /**
             * fetch admin detail
             */
            $details  = UserDetail::getUserDetails(null, null, null, null,null);

            if ($details['status'] != 200) {
                return response()->json(
                    $details
                    , $details['status']);
            }
            $adminList['email'] = $details['data']['adminEmailList'];

            foreach ($adminList['email'] as $key => $admin_email){
                if('lugmety.captain@gmail.com' === $admin_email){
                    unset($adminList['email'][$key]);
                    break;
                }

            }
            $adminList['email'][] = 'lugmety@gmail.com';
            $adminList['email'] = array_values(array_unique( $adminList['email']));
            switch ($this->action) {
                /**
                 * If action is 'contact-us-customer', send 'contact us email  created' message to admin .
                 */
                case "contact-us-customer": {
                    try {


                        //Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Email By Customer';
                        // $description = 'Restaurant of id: ' . $restaurantId . " is created";
                        $adminList['data'] = $this->mainData['data'][0];

                        $this->replyEmail = $this->mainData['data'][0]['email'];
                        $this->replyToName = $this->mainData['data'][0]['full_name'];
                        /**
                         * send email to admin using the sender email and sender name
                         */
                        $this->sendMessage('GeneralService.contact-us-customer', $adminList, $this->sender);
                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "contact-us-operator": {
                    try {


                        //Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Email By Restaurant Operator';
                        // $description = 'Restaurant of id: ' . $restaurantId . " is created";
                        $adminList['data'] = $this->mainData['data'][0];

                        $this->replyEmail = $this->mainData['data'][0]['email'];
                        $this->replyToName = $this->mainData['data'][0]['full_name'];
                        /**
                         * calling restaurant service
                         */
                        $restaurant_id = $this->mainData['data'][0]["branch_id"];
                        $code = '$2y$12$dAbXhgmUlQ1IPnRioaJt4uWScPovb/HoFD171BUPdKVrQ0mDos.B6'; //Alice123$
                        $fullUrl = Config::get('config.restaurant_base_url')."/notification/restaurant/$restaurant_id/branch?hash=$code";
                        $restaurantCall = \RemoteCall::getSpecific($fullUrl);
                        if ($restaurantCall['status'] != 200) {
                            return response()->json(
                                $restaurantCall
                                , $restaurantCall['status']);
                        }
                        $adminList['data']['branch_info'] = $restaurantCall["message"]["data"];
                        /**
                         * send email to admin using the sender email and sender name
                         */
                        $this->sendMessage('GeneralService.contact-us-operator', $adminList, $this->sender);
                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                default:
                    return false;
            }

        }
        catch (\Exception $ex){

        }
    }

    /**
     * If the service name is order service, the mails are sent according to the actions.
     *
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function orderService()
    {
        try {
            /**
             * Get $restaurantId and $driverId from $this->mainData.
             */
            $restaurantId = isset($this->mainData['data']["restaurant_id"]) ? $this->mainData['data']["restaurant_id"] : null;
            $parentId = isset($this->mainData['data']["parent_id"]) ? $this->mainData['data']["parent_id"] : null;

            $driverId = isset($this->mainData['data']["driver_id"]) ? $this->mainData['data']["driver_id"] : null;
            $countryId = (isset($this->mainData['data']["country_id"]) && !empty($this->mainData['data']["country_id"])) ? $this->mainData['data']["country_id"] : null;

            /**
             * Get all the details required from oauth service.
             * The details contains id,name, email and mobile of admin, customer and driver.
             */
            $details = UserDetail::getUserDetails($this->userId, $restaurantId, $driverId, $parentId, $countryId);

            if ($details['status'] != 200) {
                return response()->json(
                    $details
                    , $details['status']);
            }
            $user = $details['data']['customer'];
            $driver_tokens = $details['data']['driver_list'];
            $adminList['email'] = $details['data']['adminEmailList'];
            $adminList['email'][] = 'orders@lugmety.com';
            /**
             * if action is order rejected then remove email 'lugmety.captain@gmail.com' and add the email 'lugmety@gmail.com' .ie
             */
            if($this->action === 'order-rejected'){
                foreach ($adminList['email'] as $key => $admin_email){
                    if('lugmety.captain@gmail.com' === $admin_email){
                        unset($adminList['email'][$key]);
                        break;
                    }

                }
                $adminList['email'][] = 'lugmety@gmail.com';
            }
            /**
             *  else send email to lugmety.captain@gmail.com and remove email 'lugmety@gmail.com'
             */
            else{
                foreach ($adminList['email'] as $key => $admin_email){
                    if('lugmety@gmail.com' === $admin_email){
                        unset($adminList['email'][$key]);
                    }
                }
                $adminList['email'][] = 'lugmety.captain@gmail.com';
            }
            $adminList['email'] = array_values(array_unique($adminList['email']));


            /**
             * Check the actions and send mails to users, admin and driver as per need.
             */

            switch ($this->action) {

                /**
                 * If action is 'order created', send 'order created' message to customer and related admins.
                 */
                case "order-created": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));

                        $orderDetails = $this->mainData['data']['user_details'];

                        $orderDetails = $user;
                        //$orderDetails['email'] = $user['email'];
                        $orderDetails['order'] = $this->mainData['data']['order'];

                        $adminList['customer'] = $orderDetails;

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        //$description = 'Order #' . $orderDetails['order']['id'] . " has been created.";

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order has been placed successfully.';

                            $pushData['type'] = 'order created';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }

                        $orderDetails['title1'] = 'Lugmety: Your order #' . $orderDetails['order']['id'] . ' has been placed successfully.';
                        //if ($this->sendMessage('OrderService.User.OrderRequest', $orderDetails, $this->sender)) {


                        $this->insertUserNotification($orderDetails['title1']);

                        //}
                        $branchTokens = $details['data']['operatorTokens'];

                        if(count($branchTokens) > 0)
                        {
                            $this->notifyOperator($branchTokens, $orderDetails['order']['id']);
                            $this->notifyOperatorWithNewServerKey($branchTokens, $orderDetails['order']['id']);
                        }

                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */

                        if(isset($adminList['customer']["order"]["user_type"]) && ($adminList['customer']["order"]["user_type"] == 'restaurant-customer')){
                            $adminList['customer']["order"]["amount"] = $adminList['customer']["order"]["amount"] - $adminList['customer']["order"]["delivery_fee"];
                            $adminList['customer']["order"]["delivery_fee"] = 0;
                        }
                        $adminList['title1'] = 'Lugmety: New order #' . $orderDetails['order']['id'] . ' has been received.';
                        if ($this->sendMessage('OrderService.Admin.OrderRequest', $adminList, $this->sender)) {
                            $this->insertAdminNotification( $adminList['title1']);
                        }
                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }
                /**
                 * If action is 'payment successful', send 'payment successful' message to customer and related admins.
                 */

                case "monthly-invoice":{
//                    Log::info(storage_path( 'exports/test.zip'));
                    $adminEmailForCC = [];
                    foreach ( $adminList['email'] as $emails){
                        $adminEmailForCC[]["email"] = $emails;
                    }
                    //calling order service to get notification detail
                    $fullurl = "http://localhost:2222/send/invoice";
                    $request = [
                        "restaurant_id" => $this->mainData['data']["restaurant_id"],
                        "start_date" => $this->mainData['data']["date"]["start_date"],
                        "end_date" => $this->mainData['data']["date"]["end_date"]
                    ];
                    Log::info("request",[
                        serialize($request)
                    ]);
                    $getInvoice = \RemoteCall::postCallWithoutToken($fullurl,$request);
                    if($getInvoice["status"] != 200){
                        die();
                    }
                    $this->createPdfAndZip($getInvoice["message"]["data"]);
                    Log::info("response",[
                        serialize($getInvoice["message"]["invoice_info"]["data"])
                    ]);
                    $user["title1"] = "test title";
                    $user["email"] = ["mandipgiri02@gmail.com"];
                    $user["adminCC"] = $adminEmailForCC;
                    $user["invoice_month"] = Carbon::parse($this->mainData['data']["date"]["start_date"])->format('F');
                    $filename =  $user["filename"] = $getInvoice["message"]["data"]["invoice_info"]["filename"];
                    $this->sendInvoiceEmail('OrderService.Invoice.invoice', $user, $this->sender);
                    if (file_exists(storage_path("/exports/$filename.zip"))) {
                        array_map('unlink', glob(storage_path() . '/exports/*.zip'));


//            return response()->download(storage_path("/exports/$fileNameInSlug.zip"))->deleteFileAfterSend(true);
                    }
                    echo "sucessfully send email";

                }
                case "payment-successful": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $adminList['customer'] = $orderDetails;


                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Bill for Order #' . $orderDetails['order']['id'] . " has been paid successfully.";

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your payment has been received.';
                            $pushData['type'] = 'payment successful';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }

                        $orderDetails['title1'] = 'Lugmety: Your payment was successful for #' . $orderDetails['order']['id'] . ".";
                        //if ($this->sendMessage('OrderService.User.Payment', $orderDetails, $this->sender)) {

                        $this->insertUserNotification($orderDetails['title1']);

                        //}

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Payment for order #' . $orderDetails['order']['id'] . ' was successful.';

                        if ($this->sendMessage('OrderService.Admin.Payment', $adminList, $this->sender)) {

                            $this->insertAdminNotification( $adminList['title1']);

                        }
                        MailNotification::find($this->id)->delete();

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case 'order-collected-by-captain':{
                    try{

                        $captainId =  $this->mainData['data']['user_details']["id"];
                        $captainDetail = UserDetail::getUserDetails(null, null,$captainId , null, $countryId);

                        if ($captainDetail['status'] != 200) {
                            return response()->json(
                                $captainDetail
                                , $captainDetail['status']);
                        }
                        $captainToken["tokens"] = $captainDetail['data']['driver']["tokens"];
                        $driverToken["tokens"] = $details['data']['driver']["tokens"];
//                        Log::info("user info",$captainDetail["data"]);
                        $pushData['title'] = 'Payment For Order Collected By Captain';
                        $orderId =  $this->mainData['data']['order_id'];
                        if (!empty($captainToken['tokens'])) {
                            $pushData['token'] = $captainToken['tokens'];
                            $pushData['message'] = "Your have successfully collected payment for Order #$orderId.";
                            $pushData['type'] = 'order collected by captain';
                            $this->mainData['data']["user_type"] = 'driver-captain';
                            $this->mainData['data']["user_id"] = $captainId;
                            $this->mainData['data']["driver_id"] = $driverId;
                            Log::info("push notification for captain driver",$pushData);
                            $this->pushNotifyForCaptain($pushData,$this->mainData);
                            $this->pushNotifyForNewAppCaptain($pushData, $this->mainData);
                            $this->insertDriverNotification($captainId, $pushData['message']);
                        }

                        if (!empty($driverToken['tokens'])) {
                            $pushData['token'] = $driverToken['tokens'];
                            $pushData['message'] = "Payment for Order #$orderId has been successfully collected by Captain";
                            $pushData['type'] = 'order collected by captain';
                            $this->mainData['data']["user_type"] = 'driver';
                            $this->mainData['data']["user_id"] = $driverId;

                            Log::info("push notification for driver",$pushData);
                            $this->pushNotifyForCaptain($pushData,$this->mainData);
                            $this->pushNotifyForNewAppCaptain($pushData, $this->mainData);
                            $this->insertDriverNotification($driverId, $pushData['message']);
                        }

                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }
                }

                case 'order-collected-by-admin' :
                    {
                        try{

                            $captainId =  $this->mainData['data']['user_details']["id"];
                            $captainDetail = UserDetail::getUserDetails(null, null,$captainId , null, $countryId);

                            if ($captainDetail['status'] != 200) {
                                return response()->json(
                                    $captainDetail
                                    , $captainDetail['status']);
                            }
                            $captainToken["tokens"] = $captainDetail['data']['driver']["tokens"];
                            $driverToken["tokens"] = $details['data']['driver']["tokens"];
//                        Log::info("user info",$captainDetail["data"]);
                            $orderId =  $this->mainData['data']['order_id'];
                            $pushData['title'] = 'Payment For Order Collected By Admin';
                            if (!empty($captainToken['tokens'])) {
                                $pushData['token'] = $captainToken['tokens'];
                                $pushData['message'] = "Payment for Order #$orderId has been successfully collected by Admin";
                                $pushData['type'] = 'order collected by captain';
                                $this->mainData['data']["user_type"] = 'driver-captain';
                                $this->mainData['data']["user_id"] = $captainId;
                                $this->mainData['data']["driver_id"] = $driverId;
                                Log::info("push notification for captain driver",$pushData);
                                $this->pushNotifyForCaptain($pushData,$this->mainData);
                                $this->pushNotifyForNewAppCaptain($pushData, $this->mainData);
                                $this->insertDriverNotification($captainId, $pushData['message']);
                            }

                            if (!empty($driverToken['tokens'])) {
                                $pushData['token'] = $driverToken['tokens'];
                                $pushData['message'] = "Payment for Order #$orderId has been successfully collected by Admin";
                                $pushData['type'] = 'order collected by captain';
                                $this->mainData['data']["user_type"] = 'driver';
                                $this->mainData['data']["user_id"] = $driverId;

                                Log::info("push notification for driver",$pushData);
                                $this->pushNotifyForCaptain($pushData,$this->mainData);
                                $this->pushNotifyForNewAppCaptain($pushData, $this->mainData);
                                $this->insertDriverNotification($driverId, $pushData['message']);
                            }

                            MailNotification::find($this->id)->delete();


                        } catch (\Exception $ex) {
                            Log::info($ex->getMessage());
                            return false;
                        }
                    }

                case "payment-failed": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));
                        $user['payment_status'] = "failed";
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $adminList['customer'] = $orderDetails;


                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        //$description = 'Payment for Order #' . $orderDetails['order']['id'] . " has been failed.";
                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your payment has been failed.';
                            $pushData['type'] = 'payment failed';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }

                        $orderDetails['title1'] = 'Lugmety: Your Payment for order #' . $orderDetails['order']['id'] . ' is failed.';

                        // if ($this->sendMessage('OrderService.User.PaymentFailed', $orderDetails, $this->sender)) {

                        $this->insertUserNotification($orderDetails['title1']);

                        //}

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Payment for order #' . $orderDetails['order']['id'] . ' is failed.';
                        if ($this->sendMessage('OrderService.Admin.PaymentFailed', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);

                        }
                        MailNotification::find($this->id)->delete();

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "order-accepted-by-restaurant": {

                    try {
                        //Log::info(serialize([$this->subject, $this->action]));

                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $adminList['customer'] = $orderDetails;
                        //$adminList['customer'] = $user;
//                        $createdAt =  Carbon::createFromFormat('Y-m-d H:i:s', $orderDetails['order']['created_at'], 'UTC');
//                       $createdAt->setTimezone($orderDetails['order']['restaurant']['timezone']);


                        if(isset($orderDetails['order']['delivery_time']) && !empty($orderDetails['order']['delivery_time'])) {
                            $deliveryTime = Carbon::parse($orderDetails['order']['delivery_time'])->format('h:i a');
                            //$time = $this->timeDiff($createdAt, $orderDetails['order']['delivery_time']);
                            if ($orderDetails['order']['delivery_type'] != "pickup") {
                                $message = 'Your order has been accepted, estimated delivery time is ' .$deliveryTime.".";
                            } else {
                                $message = 'Your order has been accepted, estimated pickup time is '.$deliveryTime.".";
                            }

                        }
                        else{
                            $message = 'Your order has been accepted, estimated delivery time is '. $orderDetails['order']['restaurant']['approximate_delivery_time']. " mins.";
                        }

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        //$description = 'Order #' . $orderDetails['order']['id'] . " has been accepted by restaurant.";
                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = $message;
                            $pushData['type'] = 'order accepted by restaurant';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        $orderDetails['title1'] = 'Lugmety: '.$orderDetails['order']['restaurant']['translation']['en'] . ' accepted your order.';

                        if (($orderDetails['order']['delivery_type'] != "pickup") && !empty($driver_tokens)) {
                            $pushData = [];
                            $pushData['token'] = $driver_tokens;
                            $pushData['message'] = 'New order from '.$orderDetails['order']['restaurant']['address'];
                            $pushData['type'] = 'New Order';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        if(isset($orderDetails["order"]["user_type"]) && ($orderDetails["order"]["user_type"] == 'restaurant-customer')){
                            $orderDetails["order"]["amount"] = $orderDetails["order"]["amount"] - $orderDetails["order"]["delivery_fee"];
                            $orderDetails["order"]["delivery_fee"] = 0;
                        }
                        if ($this->sendMessage('OrderService.User.OrderAccepted', $orderDetails, $this->sender)) {
                            $this->insertUserNotification($orderDetails['title1']);

                        }

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Order is accepted by the restaurant ' . $orderDetails['order']['restaurant']['translation']['en'] . '.';
                        if ($this->sendMessage('OrderService.Admin.OrderAccepted', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        MailNotification::find($this->id)->delete();
                        return true;
                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "order-accepted-by-driver": {
                    try {

                        // Log::info(serialize([$this->subject, $this->action]));
                        $adminList['customer'] = $user;
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $orderDetails['driver'] = $details['data']['driver'];
                        $adminList['customer'] = $orderDetails;
                        $driver = $details['data']['driver'];
                        $driver['customer'] = array_except($orderDetails, ['driver']);


                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        //  $description = 'Order #' . $orderDetails['order']['id'] . " has been accepted by driver " . $details['data']['driver']['name'];

                        $orderDetails['title1'] = 'Lugmety: Your order #'.$orderDetails['order']['id'].' has been assigned to driver.';
                        //if ($this->sendMessage('OrderService.User.OrderAcceptedByDriver', $orderDetails, $this->sender)) {

                        $this->insertUserNotification($orderDetails['title1']);

                        //}
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: '.$driver['name'] . ' accepted your order.';
                        if ($this->sendMessage('OrderService.Admin.OrderAcceptedByDriver', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order #'. $orderDetails['order']['id'].' has been assigned to driver.';
                            $pushData['type'] = 'order accepted by driver';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $driver['title1'] = 'Lugmety: Order Accepted by Driver';
                        if ($this->sendMessage('OrderService.Driver.OrderAcceptedByDriver', $driver, $this->sender)) {

                            $this->insertDriverNotification($driverId, $driver['title1']);
                        }


                        if (!empty($driver['tokens'])) {
                            $pushData['token'] = $driver['tokens'];
                            $pushData['message'] = 'You are assigned to deliver order #'.$orderDetails['order']['id'].'.';
                            $pushData['type'] = 'order accepted by driver';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        MailNotification::find($this->id)->delete();

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "order-rejected": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));
                        $adminList['customer'] = $user;
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $orderDetails['driver'] = $details['data']['driver'];
                        $adminList['customer'] = $orderDetails;
                        $driver = $details['data']['driver'];
                        $driver['customer'] = array_except($orderDetails, ['driver']);

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Lugmety: Order #' . $orderDetails['order']['id'] . " has been rejected.";
                        $orderDetails['title1'] = 'Lugmety: '.$orderDetails['order']['restaurant']['translation']['en'] . ' did not accept your order #'.$orderDetails['order']['id'].' at this time.';
                        if ($this->sendMessage('OrderService.User.OrderRejected', $orderDetails, $this->sender)) {

                            $this->insertUserNotification($orderDetails['title1']);

                        }

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order #'.$orderDetails['order']['id'].' has been rejected.';
                            $pushData['type'] = 'order rejected';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: '.$orderDetails['order']['restaurant']['translation']['en'] . ' did not accept order #'.$orderDetails['order']['id'].' at this time.';
                        if ($this->sendMessage('OrderService.Admin.OrderRejected', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);
                        }

                        MailNotification::find($this->id)->delete();

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }

                case "order-picked": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));
                        $user['is_picked'] = 1;
                        $adminList['customer'] = $user;
                        $orderDetails = $user;
                        $orderDetails['is_picked'] = 1;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $orderDetails['driver'] = $details['data']['driver'];
                        $adminList['customer'] = $orderDetails;
                        $driver = $details['data']['driver'];
                        $driver['customer'] = array_except($orderDetails, ['driver']);


                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Order #' . $orderDetails['order']['id'] . " has been picked by driver " . $driver['name'];
                        $orderDetails['title1'] ='Lugmety: Your order #'.$orderDetails['order']['id'].' is on the move now.';

                        // if ($this->sendMessage('OrderService.User.OrderReassigned', $orderDetails, $this->sender)) {

                        $not = 'Lugmety: Your order #'.$orderDetails['order']['id'].' is on its way.You can now track your driver on a map, just go to your order history';
                        $this->insertUserNotification($not);

                        // }

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order #'.$orderDetails['order']['id'].' is on its way.You can now track your driver on a map, just go to your order history';
                            $pushData['type'] = 'order picked up';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Order #' . $orderDetails['order']['id'] . " has just picked by driver " . trim($driver['name']);
                        if ($this->sendMessage('OrderService.Admin.OrderReassigned', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);
                        }

                        MailNotification::find($this->id)->delete();

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }
                case "order-completed": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $adminList['customer'] = $orderDetails;

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Order #' . $orderDetails['order']['id'] . " has been completed";
                        $orderDetails['title1'] ='Lugmety: Your order #'.$orderDetails['order']['id'].' from '. $orderDetails['order']['restaurant']['translation']['en'].' has been delivered.';
                        //if ($this->sendMessage('OrderService.User.OrderCompleted', $orderDetails, $this->sender)) {

                        $this->insertUserNotification($orderDetails['title1']);

                        //}

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order #'.$orderDetails['order']['id'].' has been delivered. Enjoy your meal!';
                            $pushData['type'] = 'order completed';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Order #'.$orderDetails['order']['id'].' has been delivered.';
                        if ($this->sendMessage('OrderService.Admin.OrderCompleted', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }

                case "order-reassign": {
                    try {
                        //Log::info(serialize($details['data']['driver']));
                        $orderDetails = $user;
                        $orderDetails['order'] = $this->mainData['data']['order'];
                        $orderDetails['driver'] = $details['data']['driver'];
                        $adminList['customer'] = $orderDetails;

                        $driver = $details['data']['driver'];
                        $driver['customer'] = array_except($orderDetails, ['driver']);
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Order #' . $orderDetails['order']['id'] . " has been reassigned to " . $driver['name'];

                        $orderDetails['title1'] ='Lugmety: Your order #'.$orderDetails['order']['id'].' has been reassigned to '. trim($driver['name']).'.';
                        //if ($this->sendMessage('OrderService.User.OrderReassigned', $orderDetails, $this->sender)) {

                        $this->insertUserNotification($orderDetails['title1']);

                        //}

                        if (!empty($user['tokens'])) {
                            $pushData['token'] = $user['tokens'];
                            $pushData['message'] = 'Your order #'.$orderDetails['order']['id'].' has been reassigned.';
                            $pushData['type'] = 'order reassign.';

                            $this->pushNotify($pushData, $orderDetails['order']);
                            $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $adminList['title1'] ='Lugmety: Order #'.$orderDetails['order']['id'].' has been reassigned to '. trim($driver['name']).'.';
                        if ($this->sendMessage('OrderService.Admin.OrderReassigned', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $driver['title1'] = 'Lugmety: Order #'.$orderDetails['order']['id']. ' has been reassigned to you.';
                        if ($this->sendMessage('OrderService.Driver.OrderReassigned', $driver, $this->sender)) {

                            $this->insertDriverNotification($driverId, $driver['title1']);
                            if (!empty($driver['tokens'])) {
                                $pushData['token'] = $driver['tokens'];
                                $pushData['message'] = 'Your order #'.$orderDetails['order']['id'].' has been reassigned.';
                                $pushData['type'] = 'order reassign.';

                                $this->pushNotify($pushData, $orderDetails['order']);
                                $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                            }
                        };
                        MailNotification::find($this->id)->delete();


                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }

                case "payout-created-by-admin": {
                    try {
                        //Log::info(serialize($details['data']['driver']));
                        $driverDetails = $details['data']['driver'];

                        $driverDetails['payouts'] = $this->mainData['data']['payouts'];
                        $driverDetails['country_info'] = $this->mainData['data']['country_info'];


                        $adminList['driver'] = $driverDetails;

                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        //$description = 'Payout of amount: ' . $driverDetails['country_info']['currency_symbol'] . ": " . $driverDetails['payouts']['amount'] . " has been created";

                        $adminList['title1'] = 'Lugmety: Admin has just initiated the payout for #'.$driverDetails['id'].'.';

                        if ($this->sendMessage('OrderService.Admin.PayoutCreated', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $driverDetails['title1'] = 'Lugmety: Admin has just initiated your payout.';
                        if ($this->sendMessage('OrderService.Driver.PayoutCreated', $driverDetails, $this->sender)) {

                            $this->insertDriverNotification($driverId, $driverDetails['title1']);
                            if (!empty($driver['tokens'])) {
                                $pushData['token'] = $driver['tokens'];
                                $pushData['message'] = 'Your Payout has been created.';
                                $pushData['type'] = 'Payout created';

                                $this->pushNotify($pushData, $driverDetails);
                                $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                            }
                        };
                        MailNotification::find($this->id)->delete();


                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }

                case "payout-status-updated-by-admin": {
                    try {
                        //Log::info(serialize($details['data']['driver']));
                        $driverDetails = $details['data']['driver'];
                        $driverDetails['update'] = 1;

                        $driverDetails['payouts'] = $this->mainData['data']['payouts'];
                        $driverDetails['country_info'] = $this->mainData['data']['country_info'];


                        $adminList['driver'] = $driverDetails;


                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        // $description = 'Payout of amount: ' . $driverDetails['country_info']['currency_symbol'] . ": " . $driverDetails['payouts']['amount'] . " has been updated to " . $driverDetails['payouts']['status'];

                        $adminList['title1'] = 'Lugmety: Admin adjusted the payout status for '.trim($driverDetails['name']).' #'.$driverDetails['id'].'.';

                        if ($this->sendMessage('OrderService.Admin.PayoutCreated', $adminList, $this->sender)) {

                            $this->insertAdminNotification($adminList['title1']);
                        }
                        /**
                         * Send mail to user and call insertUserNotification() to insert data to user_notifications table.
                         */
                        $driverDetails['title1'] = 'Lugmety: Admin adjusted the payout status to #'. $driverDetails['payouts']['status'].' for you.';
                        if ($this->sendMessage('OrderService.Driver.PayoutCreated', $driverDetails, $this->sender)) {

                            $this->insertDriverNotification($driverId, $driverDetails['title1']);
                            if (!empty($driver['tokens'])) {
                                $pushData['token'] = $driver['tokens'];
                                $pushData['message'] = 'Your payout status updated to ' . $driverDetails['payouts']['status'];
                                $pushData['type'] = 'Payout Status Updated ';

                                $this->pushNotify($pushData, $driverDetails);
                                $this->pushNotifyForNewApp($pushData, $orderDetails['order']);
                            }
                        };
                        MailNotification::find($this->id)->delete();


                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }


                }

                default:
                    return false;
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            return false;
        }


    }

    public function restaurantService()
    {
        try {
            $restaurantId = $this->mainData['data']["restaurant"]['id'];
            $parentId = isset($this->mainData['data']["restaurant"]["parent_id"]) ? $this->mainData['data']["restaurant"]["parent_id"] : null;


            switch ($this->action) {
                /**
                 * If action is 'order created', send 'order created' message to customer and related admins.
                 */
                case "restaurant-created": {
                    try {
                        $details = UserDetail::getUserDetails(null, null, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];


                        //Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        $adminList['title1'] = 'Lugmety: Restaurant '.$adminList['restaurant']['translation']['en']['name'].' #'.$restaurantId .' has been created.';
                        // $description = 'Restaurant of id: ' . $restaurantId . " is created";
                        if ($this->sendMessage('RestaurantService.Admin.Create', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        MailNotification::find($this->id)->delete();
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }
                /**
                 * If action is 'payment successful', send 'payment successful' message to customer and related admins.
                 */
                case "restaurant-updated": {
                    try {
                        $details = UserDetail::getUserDetails(null, null, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];

                        // Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        //$description = 'Restaurant of id: ' . $restaurantId . " is updated";
                        $adminList['title1'] = 'Lugmety: Restaurant '.$adminList['restaurant']['translation']['en']['name'].' #'.$restaurantId .' has been updated.';
                        if ($this->sendMessage('RestaurantService.Admin.Update', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);

                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }
                case "restaurant-deleted": {
                    try {
                        $details = UserDetail::getUserDetails(null, null, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];
                        //Log::info(serialize([$this->subject, $this->action]));
                        //  $user['email'] = 'check@gmail.com';

                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        //$description = 'Restaurant of id: ' . $restaurantId . " is deleted";
                        $adminList['title1'] = 'Lugmety: Restaurant '.$adminList['restaurant']['name'].' #'.$restaurantId .' has been deleted.';
                        if ($this->sendMessage('RestaurantService.Admin.Delete', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "restaurant-branch-created": {
                    try {
                        $details = UserDetail::getUserDetails(null, $restaurantId, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];


                        //Log::info(serialize([$this->subject, $this->action]));

                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        // $description = 'Restaurant Branch of id: ' . $restaurantId . " is created";
                        $adminList['title1'] = 'Lugmety: Restaurant Branch '.$adminList['restaurant']['translation']['en']['name'].' #'.$restaurantId .' has been created.';
                        if ($this->sendMessage('RestaurantService.Admin.BranchCreate', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }
                }

                case "restaurant-branch-updated": {
                    try {
                        $details = UserDetail::getUserDetails(null, $restaurantId, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];

                        //Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        // $description = 'Restaurant Branch of id: ' . $restaurantId . " is updated";
                        $adminList['title1'] = 'Lugmety: Restaurant Branch '.$adminList['restaurant']['translation']['en']['name'].' #'.$restaurantId .' has been updated.';
                        if ($this->sendMessage('RestaurantService.Admin.BranchUpdate', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "restaurant-branch-deleted": {
                    try {
                        $details = UserDetail::getUserDetails(null, $restaurantId, null, null);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $adminList['restaurant'] = $this->mainData['data']['restaurant'];

                        //Log::info(serialize([$this->subject, $this->action]));


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        //  $description = 'Restaurant Branch of id: ' . $restaurantId . " is deleted";
                        $adminList['title1'] = 'Lugmety: Restaurant Branch '.$adminList['restaurant']['translation']['en']['name'].' #'.$restaurantId .' has been deleted.';
                        if ($this->sendMessage('RestaurantService.Admin.BranchDelete', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }

                }

                case "restaurant-reservation-created": {
                    try {
                        // Log::info(serialize([$this->subject, $this->action]));

                        $details = UserDetail::getResAdminDetails(null, $restaurantId, null, $parentId);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        // $adminList['restaurant'] = $this->mainData['data']['restaurant'];


                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        $user = $this->mainData['data']['reserver'];

                        $user['restaurant'] = $this->mainData['data']['restaurant'];
                        $adminList['customer'] = $user;

                        // $description = "Reservation in restaurant: " . $user['restaurant']['name'] . "has been requested";

                        $user['title1'] = 'Lugmety: You just created a reservation at '.$user['restaurant']['name'].'.';
                        if ($this->sendMessage('RestaurantService.User.ReservationRequest', $user, $this->sender)) {
                            $this->insertUserNotification($user['title1']);
                        }
                        $adminList['title1'] = 'Lugmety: New Reservation has been created at '.$user['restaurant']['name'].' by '. trim($user['name']).'.';
                        if ($this->sendMessage('RestaurantService.Admin.ReservationRequest', $adminList, $this->sender)) {
                            $this->insertAdminNotification($adminList['title1']);
                        }
                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        Log::info($ex->getMessage());
                        return false;
                    }
                }

                case "restaurant-reservation-verified": {
                    try {
                        //Log::info(serialize([$this->subject, $this->action]));

                        $details = UserDetail::getResAdminDetails(null, $restaurantId, null, $parentId);

                        if ($details['status'] != 200) {
                            return false;
                        }
                        $adminList['email'] = $details['data']['adminEmailList'];
                        $user = $this->mainData['data']['reserver'];
                        $user['restaurant'] = $this->mainData['data']['restaurant'];
                        $adminList['customer'] = $user;

                        /**
                         * Send mail to all related admins and call insertAdminNotification() to insert data to admin_notifications table.
                         */
                        //$description = "Reservation in restaurant: " . $user['restaurant']['name'] . "has been verified";

                        $adminList['title1'] = 'Lugmety: Reservation followup for '.trim($user['name']).' at '.$user['restaurant']['name'].'.';
                        if ($this->sendMessage('RestaurantService.Admin.ReservationVerified', $adminList, $this->sender)) {
                            $this->insertAdminNotification( $adminList['title1']);
                        }

                        $user['title1'] = 'Lugmety: Follow up on your reservation at '.$user['restaurant']['name'].'.';
                        if ($this->sendMessage('RestaurantService.User.ReservationVerified', $user, $this->sender)) {
                            $this->insertUserNotification($user['title1']);
                        }

                        \App\Models\EmailNotification::destroy($this->id);
                        return true;

                    } catch (\Exception $ex) {
                        // Log::info($ex->getMessage());
                        return false;
                    }
                }


                default:
                    return false;
            }
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            return false;
        }
    }

    public function oauthService()
    {
        try {
            $action = $this->action;
            $data = $this->mainData["data"];
            $userDetails = $data['user_details'];
            $user['name'] = preg_replace('/\s+/', ' ',trim(implode(" ", array($userDetails['first_name'], $userDetails['middle_name'], $userDetails['last_name']))));
//        $adminEmailList = explode(",", \Redis::get("admin-list"));
            switch ($action) {

                case "customer-create": {


                    $user["url"] = $data["confirmation_url"];
                    $user["email"] = $data["user_details"]["email"];
                    $template = "oauthService.userCreation";
                    $user['title1'] = 'Lugmety: Your Activation Code';
                    $this->sendMessage($template, $user, $this->sender);

//                $content = "User Created";
                    $user["url"] = $data["confirmation_url"];
//                $user["email"] = $data["user_details"]["email"];
//                $template = "oauthService.userCreation";
//                    $sender = "Lugmety@noreply.com";
                    //$this->insertUserNotification();
//                if($this->sendMessage($template,$user,$sender)) $this->insertUserNotification();
                    /*
                   * admin part
                   */
                    $details = UserDetail::getUserDetails(null, null, null, null);

                    if ($details['status'] != 200) {
                        return false;
                    }
                    $user["email"] = $details['data']['adminEmailList'];
                    $user["id"] = $userDetails["id"];
                    //Log::info(serialize($user["email"]));
                    $adminTemplate = "oauthService.admin.userCreation";
                    $user['title1'] = 'Lugmety: New customer #'.$user['id']. ' has been created.';
                    $this->sendMessage($adminTemplate, $user, $this->sender);
                    \App\Models\EmailNotification::destroy($this->id);

                    return true;

                }


                case "sendcodeinemail": {
                    Log::info("resend code in email");
                    $user["url"] = $data["confirmation_url"];
                    $user["email"] = $data["user_details"]["email"];
                    $template = "oauthService.sendCodeInEmail";
                    $user['title1'] = 'Lugmety: Your Activation Code';
                    $this->sendMessage($template, $user, $this->sender);
                    \App\Models\EmailNotification::destroy($this->id);

                }

                case "driver-create": {
                    $content = "Driver Created";
                    $user["email"] = $data["user_details"]["email"];
                    $template = "oauthService.driverCreation";
                    // $sender = "Lugmety@noreply.com";
                    $user["id"] = $userDetails["id"];
                    $user['title1'] = 'Lugmety: New driver #'.$user['id']. ' has been created.';
                    $this->sendMessage($template, $user, $this->sender);
                    $details = UserDetail::getUserDetails(null, null, null, null);
                    Log::info("new_driver_creation",[$details]);
                    if ($details['status'] != 200) {
                        return false;
                    }
                    $user["email"] = $details['data']['adminEmailList'];
                    $title = "Lugmety: New driver" . $user['name'] . " has been created.";
                    /*
                     * admin part
                     */
                    //Log::info(serialize($user["email"]));
                    $adminTemplate = "oauthService.admin.driverCreation";

                    $this->sendMessage($adminTemplate, $user, $this->sender);
                    \App\Models\EmailNotification::destroy($this->id);

                    return true;

                }

//                case "update-by-user": {
//                    $user["email"] = $data["user_details"]["email"];
//                    $user["password"] = $data["password"];
//
//                    $template = "oauthService.updateByUser";
//                    $sender = "Lugmety@noreply.com";
//                    if ($this->sendMessage($template, $user, $sender)) $this->insertUserNotification();
//                    /**
//                     * admin part
//                     */
//                    $user["email"] = $this->redisDataAdminList;
//                    $user["id"] = $userDetails["id"];
//                    $adminTemplate = "oauthService.admin.updateByUser";
//                    if ($this->sendMessage($adminTemplate, $user, $sender)) $this->insertAdminNotification();
//                    \App\Models\EmailNotification::destroy($this->id);
//
//                    return true;
//
//                }

//                case "customer-update-by-admin": {
//                    $user["email"] = $data["user_details"]["email"];
//                    $user["activate"] = $data["activate"];
//                    $template = "oauthService.updateCustomerByAdmin";
//                    $sender = "Lugmety@noreply.com";
//                    if ($this->sendMessage($template, $user, $sender)) $this->insertUserNotification();
//                    /**
//                     * admin part
//                     */
//                    $user["email"] = $this->redisDataAdminList;
//                    $user["id"] = $userDetails["id"];
//                    Log::info(serialize($user["email"]));
//                    $adminTemplate = "oauthService.admin.updateCustomerByAdmin";
//                    if ($this->sendMessage($adminTemplate, $user, $sender)) $this->insertAdminNotification();
//                    \App\Models\EmailNotification::destroy($this->id);
//
//                    return true;
//
//                }

//                case "driver-update-by-admin": {
//                    $user["email"] = $data["user_details"]["email"];
//                    $user["activate"] = $data["activate"];
//                    $template = "oauthService.updateDriverByAdmin";
//                    $sender = "Lugmety@noreply.com";
//                    if ($this->sendMessage($template, $user, $sender)) $this->insertUserNotification();
//                    /**
//                     * admin part
//                     */
//                    $user["email"] = $this->redisDataAdminList;
//                    $user["id"] = $userDetails["id"];
//                    Log::info(serialize($user["activate"]));
//                    $adminTemplate = "oauthService.admin.updateDriverByAdmin";
//                    if ($this->sendMessage($adminTemplate, $user, $sender)) $this->insertAdminNotification();
//                    \App\Models\EmailNotification::destroy($this->id);
//                    return true;
//
//                }
//                case "restaurant-admin-update-by-admin": {
//                    $user["email"] = $data["user_details"]["email"];
//                    $user["activate"] = $data["activate"];
//                    $user["role"] = $data["role"];
//                    $template = "oauthService.updateRestaurantAdminByAdmin";
//                    $sender = "Lugmety@noreply.com";
//                    if ($this->sendMessage($template, $user, $sender)) $this->insertUserNotification();
//                    /**
//                     * admin part
//                     */
//                    $user["email"] = $this->redisDataAdminList;
//                    $user["id"] = $userDetails["id"];
//                    Log::info(serialize($user["email"]));
//                    $adminTemplate = "oauthService.admin.updateRestaurantAdminByAdmin";
//                    if ($this->sendMessage($adminTemplate, $user, $sender)) $this->insertAdminNotification();
//                    \App\Models\EmailNotification::destroy($this->id);
//
//                    return true;
//
//                }


                case "forgot-password": {
                    $user["url"] = $data["confirmation_url"];
                    $user["email"] = $data["user_details"]["email"];
                    $template = "oauthService.forgotPassword";
                    $user['title1'] = 'Lugmety: Your Lugmety Password Reset';
                    if ($this->sendMessage($template, $user, $this->sender) == true){
                        // $this->insertUserNotification($user['title1']);
                        // Log::info("sucess");

                    };
//                    /**
//                     * admin part
//                     */
//                    $details = UserDetail::getUserDetails(null, null, null, null);
//
////
//                    if ($details['status'] != 200) {
//                        return false;
//                    }
//                    $user["email"] = $details['data']['adminEmailList'];
//                    $user["id"] = $userDetails["id"];
//                    Log::info(serialize($user["email"]));
//                    $user['title1'] = 'Lugmety: Lugmety Password Reset';
//                    $adminTemplate = "oauthService.admin.forgotPassword";
//                    $this->sendMessage($adminTemplate, $user, $this->sender);
                    \App\Models\EmailNotification::destroy($this->id);

                    return true;
                }

                case "customer-verification": {
//                $user["url"] = $data["confirmation_url"];
                    $user["email"] = $data["user_details"]["email"];
                    $template = "oauthService.userActivated";
                    $this->sendMessage($template, $user, $this->sender);
                    /**
                     * admin part
                     */
                    $details = UserDetail::getUserDetails(null, null, null, null);

                    if ($details['status'] != 200) {
                        return false;
                    }
                    $user["email"] = $details['data']['adminEmailList'];
                    $user["id"] = $userDetails["id"];
                    //Log::info(serialize($user["email"]));
                    $adminTemplate = "oauthService.admin.userActivated";
                    $user['title1'] = 'Lugmety: Customer Verification';
                    $this->sendMessage($adminTemplate, $user, $this->sender);
                    \App\Models\EmailNotification::destroy($this->id);

                    return true;
                }

                default:
                    return false;
            }
        } catch (\Exception $ex) {
            return false;
        }

    }

    /**
     * Insert notification data of user into user_notifications.
     * @return bool
     */
    private function insertUserNotification($description)
    {
        try {
            $create['user_id'] = $this->userId;
            $create['title'] = $this->subject == "Order Picked"? "Order Picked Up": $this->subject;
            $create['data'] = serialize($this->mainData);
            $create['status'] = '0';
            $create['description'] = trim(str_replace('Lugmety: ', '', $description));
            $user = UserNotification::create($create);


            return true;
        }
        catch (\Exception $ex){
            Log::error($ex->getMessage());
        }

    }

    /**
     * Insert notification data of admin into admin_notifications.
     * @return bool
     */
    private function insertAdminNotification($description)
    {

        $create['title'] = $this->subject == "Order Picked"? "Order Picked Up": $this->subject;
        $create['data'] = serialize($this->mainData);
        $create['description'] = trim(str_replace('Lugmety: ','',$description));


        AdminNotification::create($create);

        return true;
    }

    /**
     * Save notification data of driver in driver_notifications.
     * @param $driverId
     * @return bool
     */

    private function insertDriverNotification($driverId, $description)
    {
        $create['user_id'] = $driverId;
        $create['title'] = $this->subject;
        $create['data'] = serialize($this->mainData);
        $create['status'] = '0';
        $create['description'] = trim(str_replace('Lugmety: ','',$description));
        DriverNotification::create($create);

        return true;
    }

    /**
     * function sends push notifications to the users having tokens using firebase.
     * @param $pushData
     * @param $order
     * @return bool
     */
    public function pushNotify($pushData, $order)
    {
        //Log::info(serialize($pushData));
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        if($pushData['type'] == 'New Order')
        {
            $sub = 'New Order';

        }else{
            $sub = $this->subject == "Order Picked"? "Order Picked Up": $this->subject;
        }

        $notificationBuilder = new PayloadNotificationBuilder($sub);
        $notificationBuilder->setBody($pushData['message'])
            ->setTag($order['id'])
            ->setTitle($sub)
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData([
            'order_id' => $order['id'],
            'type' => $sub,
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $pushData['token'];

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        Log::info("push_notification_v1", [ 'receiver_token' => $token, 'order_id' => $order['id'], 'response' => $downstreamResponse ]);

        // Log::info(serialize($downstreamResponse));

        return true;

    }

    public function pushNotifyForCaptain($pushData, $notificationData)
    {
        //Log::info(serialize($pushData));
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $sub =
//        if($pushData['type'] == 'New Order')
//        {
//            $sub = 'New Order';
//
//        }else{
//            $sub = $this->subject == "Order Picked"? "Order Picked Up": $this->subject;
//        }
        $sub = $pushData["message"];
        $notificationBuilder = new PayloadNotificationBuilder($sub);
        $notificationBuilder->setBody($pushData['message'])
            ->setTag($notificationData['data']['order_id'])
            ->setTitle($pushData["title"])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();
        if(isset($notificationData["data"]["driver_id"])){
            $dataBuilder->addData([
                'order_id' => $notificationData["data"]['order_id'],
                'type' => $sub,
                'user_type' => $notificationData["data"]["user_type"],
                'user_id' => $notificationData["data"]["user_id"],
                'driver_id' => $notificationData["data"]["driver_id"]
            ]);
        }
        else{
            $dataBuilder->addData([
                'order_id' => $notificationData["data"]['order_id'],
                'type' => $sub,
                'user_type' => $notificationData["data"]["user_type"],
                'user_id' => $notificationData["data"]["user_id"]
            ]);
        }


        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $token = $pushData['token'];

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

        Log::info("push_notification_captain_v1", [ 'receiver_token' => $token, 'order_id' => $notificationData["data"]['order_id'] ,"downstream" => serialize($downstreamResponse)]);

        // Log::info(serialize($downstreamResponse));

        return true;

    }

    public function pushNotifyForNewAppCaptain($pushData,$data){
        $sub = $pushData["title"];

        $msg = array
        (
            'content' 	=> $pushData['message'],
            'title'		=> $sub,
            'order_id'	=>  $data['data']['order_id'],
            'country_id' => $data['data']['country_id'],
            'user_type' =>$data['data']["user_type"],
            'user_id' =>$data['data']["user_id"],
            'type' => $this->action,
            'tickerText'	=> '',
            'vibrate'	=> 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon'
        );
        if(isset($data["data"]["driver_id"])){
            $msg["driver_id"] = $data["data"]["driver_id"];
        }


        $fields = array
        (
            'registration_ids' 	=> $pushData['token'],
            "priority" => "high",
            "content_available" => true,
            'data'			=> $msg
        );

        $headers = array
        (
            'Authorization: key='. Config::get('fcm.http.server_key1'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );

        Log::info("push_notification_captain_v2", [ 'receiver_token' => $pushData['token'], 'content' => $fields,'headers' => $headers, 'response' => $result ]);


        // Log::info(serialize($result));
        curl_close( $ch );
        return true;
    }

    private function pushNotifyForNewApp($pushData, $order)
    {
        if($pushData['type'] == 'New Order')
        {
            $sub = 'New Order';

        }else{
            $sub = $this->subject == "Order Picked"? "Order Picked Up": $this->subject;
        }
        $msg = array
        (
            'content' 	=> $pushData['message'],
            'title'		=> $sub,
            'order_id'	=>  $order['id'],
            'country_id' => $order['country_id'],
            'type' => $this->action,
            'tickerText'	=> '',
            'vibrate'	=> 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon'
        );


        $fields = array
        (
            'registration_ids' 	=> $pushData['token'],
            "priority" => "high",
            "content_available" => true,
            'data'			=> $msg
        );

        $headers = array
        (
            'Authorization: key='. Config::get('fcm.http.server_key1'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );

        Log::info("push_notification_v2", [ 'receiver_token' => $pushData['token'], 'content' => $fields,'headers' => $headers, 'response' => $result ]);


        // Log::info(serialize($result));
        curl_close( $ch );
        return true;
    }

    private function notifyOperator($branchTokens, $orderId)
    {

        $msg = array
        (
            'message' 	=> 'New order #' . $orderId . ' has been received.',
            'title'		=> 'New order #' . $orderId . ' has been received.',
            'order_id'	=> $orderId,
            'tickerText'	=> '',
            'vibrate'	=> 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon'
        );
        $fields = array
        (
            'registration_ids' 	=> $branchTokens,
            "priority" => "high",
            "content_available" => true,
            'data'			=> $msg
        );

        $headers = array
        (
            'Authorization: key='. Config::get('fcm.http.server_key'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );

        Log::info(serialize($result));
        curl_close( $ch );
        return true;
    }

    private function notifyOperatorWithNewServerKey($branchTokens, $orderId)
    {

        $msg = array
        (
            'message' 	=> 'New order #' . $orderId . ' has been received.',
            'title'		=> 'New order #' . $orderId . ' has been received.',
            'order_id'	=> $orderId,
            'tickerText'	=> '',
            'vibrate'	=> 1,
            'sound'		=> 1,
            'largeIcon'	=> 'large_icon',
            'smallIcon'	=> 'small_icon'
        );
        $fields = array
        (
            'registration_ids' 	=> $branchTokens,
            "priority" => "high",
            "content_available" => true,
            'data'			=> $msg
        );

        $headers = array
        (
            'Authorization: key='. Config::get('fcm.http.server_key1'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch );

        Log::info(serialize($result));
        curl_close( $ch );
        return true;
    }

    private function createPdfAndZip($data){
        echo "here";
        $html2pdf = new Mpdf(['tempDir' => storage_path() . "/exports"]);
        $content = view("OrderService.Invoice.invoice-en")->with(["data"=> $data["invoice_info"]]);

        $html2pdf->autoScriptToLang = true;
        $html2pdf->autoLangToFont = true;
        $html2pdf->WriteHTML($content);
        $html2pdf->output(storage_path() . "/exports/invoice-en". ".pdf");
        $html2pdf1 = new Mpdf(['tempDir' => storage_path() . "/exports"]);
        $html2pdf1->autoScriptToLang = true;
        $html2pdf1->autoLangToFont = true;
        $contentInArabic = view("OrderService.Invoice.invoice-ar")->with(["data"=> $data["invoice_info"]]);
        $html2pdf1->writeHTML($contentInArabic);
        $fileNameInSlug =$data["invoice_info"]["invoice_info"]["filename"];
        $this->excelHelper->createSalesReport($data["orders"],total,$fileNameInSlug);
        $html2pdf1->output(storage_path() . "/exports/invoice-ar". ".pdf");
        if (is_dir(storage_path() . '/exports/ttfontdata')) {
            array_map('unlink', glob(storage_path() . '/exports/ttfontdata/*'));
            rmdir(storage_path() . '/exports/ttfontdata');
        }
        $zipper = new Zipper();
        $files = glob(storage_path() . '/exports/*');
        $zipper->make(storage_path("/exports/$fileNameInSlug.zip"))->add($files)->close();
        if (file_exists(storage_path("/exports/$fileNameInSlug.zip"))) {
            array_map('unlink', glob(storage_path() . '/exports/*.pdf'));
            array_map('unlink', glob(storage_path() . '/exports/*.xls'));
            array_map('unlink', glob(storage_path() . '/exports/*.csv'));

//            return response()->download(storage_path("/exports/$fileNameInSlug.zip"))->deleteFileAfterSend(true);
        }
        return true;
    }



}