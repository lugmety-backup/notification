<?php

namespace App\Http\Controllers\NotificationClass;

use App\Http\Controllers\NotificationClass\ProcessInterface;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Twilio\Rest\Client;
use UserDetail;


/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/28/17
 * Time: 3:47 PM
 */
class SmsNotification implements ProcessInterface, SupportedMethodInterface
{
    protected $serviceName;
    protected $email;
    protected $subject;
    protected $userId;
    protected $mainData;
    protected $sender;
    protected $client;
    protected $id;
    protected $mobileNumber;

    public function __construct($emailData)
    {
        $this->id = $emailData['id'];
        $this->userId = $emailData["user_id"];
        $this->mobileNumber = $emailData["mobile_no"];
        $this->mainData = unserialize($emailData["data"]);
        $this->serviceName = $this->mainData["service"];
        $this->action = $this->mainData['action'];
        $this->sender = Config::get('config.twilio_number');
        $this->client = new Client();
        $this->callFunction();


    }

    private function callFunction()
    {
        if ($this->serviceName == "order service") $this->orderService();
        elseif ($this->serviceName == "oauth service") $this->oauthService();
        elseif ($this->serviceName == "restaurant service") $this->restaurantService();
    }

    public function sendMessage($messageBody,$user =null, $sender)
    {
        try {
            $message = $this->client->messages->create(
                $this->mobileNumber, // Text this number
                array(
                    'from' => $sender, // From a valid Twilio number
                    'body' => $messageBody
                )
            );
            return true;
        } catch (\Exception $ex) {
            Log::error(json_encode(['status' => '400', 'message' => $ex->getMessage()]));
            return false;
        }

    }

    public function orderService()
    {
//        $restaurantId = $this->mainData['data']["restaurant_id"];
//        $driverId = isset($this->mainData['data']["driver_id"]) ? $this->mainData['data']["driver_id"] : null;
//
//        $details = UserDetail::getUserDetails($this->userId, $restaurantId, $driverId);
//
//        if ($details['status'] != 200) {
//            return response()->json(
//                $details
//                , $details['status']);
//        }
//        $user = $details['data']['customer'];
//
//        switch ($this->action) {
//            case "order created": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Thank you for your order";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//
//            }
//            case "payment successful": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Your payment is successful";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//            }
//
//            case "order accepted by restaurant": {
//
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Your order has been accepted by restaurant";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//
//            }
//
//            case "order accepted by driver": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Your order has been accepted by driver";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//
//            }
//
//            case "order rejected": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "sorry, Your order has been rejected by restaurant";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//
//
//            }
//
//            case "order completed": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Your order has been completed";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//            }
//
//            case "order reassign": {
//                try {
//                    //$adminList['customer'] = $user;
//                    $messageBody = "Your order has been reassigned to driver ....";
//
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if ($this->sendMessage($messageBody, $user, $this->sender)) {
//                        \App\Models\SmsNotification::destroy($this->id);
//                        return true;
//                    } else {
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//
//                    return false;
//                }
//            }
//
//            default:
//                return false;
//        }
    }

    public function restaurantService()
    {
        // TODO: Implement restaurantService() method.
    }

    public function oauthService()
    {
        $action = $this->action;
        $data = $this->mainData["data"];
        $userDetails = $data['user_details'];
//        $user['mobile'] = '+9779840072010';
        $user['name'] = implode(" ", array($userDetails['first_name'], $userDetails['middle_name'], $userDetails['last_name']));
//        $adminEmailList = explode(",", \Redis::get("admin-list"));
        switch ($action) {

            case "customer-create": {
                try {
                    $url = $data["confirmation_url"];
                    $messageBody = "Thank you for your registration. Use this $url to activate your account";

                    Log::info(Config::get('config.twilio_number'));

                    if($this->sendMessage($messageBody, $user, $this->sender))
                    {
                        \App\Models\SmsNotification::find($this->id)->delete();
                        return true;
                    }else{
                        throw new \Exception();
                    }


                } catch (\Exception $ex) {
                    Log::info($ex->getMessage());
                    return false;
                }


            }

//            case "customer verification": {
//                try {
//
//                    $messageBody = "Thank you for registration.Your account has been successfully activated";
//
////                    Log::info(Config::get('config.twilio_number'));
//
//                    if($this->sendMessage($messageBody, $user, $this->sender))
//                    {
//                        \App\Models\SmsNotification::find($this->id)->delete();
//                        return true;
//                    }else{
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//                    Log::info($ex->getMessage());
//                    return false;
//                }


//            }

            case "driver-create": {
                $messageBody = "Thank you for applying the position of driver in Lugmety. Please allow us 2 - 3 days to go with the verification and the activation process.";

                Log::info(Config::get('config.twilio_number'));

                if($this->sendMessage($messageBody, $user, $this->sender))
                {
                    \App\Models\SmsNotification::find($this->id)->delete();
                    return true;
                }else{
                    throw new \Exception();
                }
//
            }
//            case "update by user": {
//                try {
//                    if($data["password"] =="1")
//                        $messageBody = "Your profile and password has been updated successfully.";
//                    else
//                        $messageBody = "Your profile has been updated successfully.";
//                    Log::info(Config::get('config.twilio_number'));
//
//                    if($this->sendMessage($messageBody, $user, $this->sender))
//                    {
//                        \App\Models\SmsNotification::find($this->id)->delete();
//                        return true;
//                    }else{
//                        throw new \Exception();
//                    }
//
//
//                } catch (\Exception $ex) {
//                    Log::info($ex->getMessage());
//                    return false;
//                }
//
//            }
//
//            case "customer update by admin": {
//                $user["email"] = $data["user_details"]["email"];
//                $user["activate"] = $data["activate"];
//                $messageBody ="Your profile  has been updated successfully by admin.";
////                "Your profile  has been updated successfully by admin.@if($user[\"activate\"] == \"1\") Also, your account been activated. @elseif($user[\"activate\"] == \"0\") Also, your account been deactivated. @endif";
//                if( $user["activate"] =="1")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been activated.";
//                elseif($user["activate"] =="0")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been deactivated.";
//                Log::info(Config::get('config.twilio_number'));
//
//                if($this->sendMessage($messageBody, $user, $this->sender))
//                {
//                    \App\Models\SmsNotification::find($this->id)->delete();
//                    return true;
//                }else{
//                    throw new \Exception();
//                }
//
//
//            }

//            case "driver update by admin": {
////                $user["email"] = $data["user_details"]["email"];
//                $user["activate"] = $data["activate"];
//                $messageBody ="Your profile has been updated successfully by admin.";
//
//                if( $user["activate"] =="1")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been activated as Driver.";
//                elseif($user["activate"] =="0")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been deactivated with role Driver.";
//                Log::info(Config::get('config.twilio_number'));
//
//                if($this->sendMessage($messageBody, $user, $this->sender))
//                {
//                    \App\Models\SmsNotification::find($this->id)->delete();
//                    return true;
//                }else{
//                    throw new \Exception();
//                }
//
//            }
//            case "restaurant admin update by admin": {
////                $user["email"] = $data["user_details"]["email"];
//                $user["activate"] = $data["activate"];
//                $user["role"] =  $data["role"];
//                $messageBody ="Your profile  has been updated successfully by admin. Also, Your role is set to ".$user["role"].".";
//                if( $user["activate"] =="1")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been activated as ".$user["role"].".";
//                else if($user["activate"] =="0")
//                    $messageBody ="Your profile  has been updated successfully by admin. Also, your account been deactivated with role ".$user["role"].".";
//                Log::info(Config::get('config.twilio_number'));
//
//                if($this->sendMessage($messageBody, $user, $this->sender))
//                {
//                    \App\Models\SmsNotification::find($this->id)->delete();
//                    return true;
//                }else{
//                    throw new \Exception();
//                }
//
//            }


//            case "forgot-password": {
//                $user["url"] = $data["confirmation_url"];
//                $messageBody = "To reset your password, Please click the link below or copy and paste it in the browser address bar. This link is valid for a day only. The link is ". $user["url"];
//                Log::info(Config::get('config.twilio_number'));
//
//                if($this->sendMessage($messageBody, $user, $this->sender))
//                {
//                    \App\Models\SmsNotification::find($this->id)->delete();
//                    return true;
//                }else{
//                    throw new \Exception();
//                }
//
//            }
//
            default:
                return false;
        }
    }
}