<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/27/18
 * Time: 11:02 AM
 */

namespace App\Http\Controllers\NotificationClass;

use App\Models\UserNotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\Topics;
use RemoteCall;
use Illuminate\Support\Facades\Config;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class CustomPushNotification implements ProcessInterface
{
    private $id;

    private $action;
    private $data;
    private $role;
    private $title;
    private $content;
    private $countryId;
    private $targetedUser;


    public function __construct($data)
    {
        $this->id = $data['id'];
        $this->action = $data['action'];
        $this->data = $data;
        $this->role = $data['user_role'];
        $this->title = $data['title'];
        $this->content = $data['content'];
        $this->countryId = $data['country_id'];
        $this->targetedUser = $data['targeted_user'];

        $this->sortMessage();


    }

    /**
     * Call functions according to the service name.
     */
    private function sortMessage()
    {
        $request['encoded_string'] = $this->encodeString('UserTokens-' . $this->role);
        $request['country_id'] = $this->countryId;

        $details = RemoteCall::store(Config::get("config.oauth_base_url") . "/notification/tokens/$this->role", $request);
        Log::info("token_request_for_custom_notification",["request" =>$request ,$details,"role" => $this->role]);
        if ($details['status'] != 200) {
            return false;
        }


        //switch ($this->action) {
        //case "new-restaurant": {
        if ($this->targetedUser == "logged-in-users") {
            Log::info("logged in user");
            if (count($details['message']['data']['tokens']) > 0) {
                $split_array_chunks = array_chunk($details['message']['data']['tokens'],900);
//                Log::info('chunks_tokens')
                foreach ($split_array_chunks as $key => $tokens){
                    $this->sendMessageWithNewKeyAndToken($this->data, $tokens);
                    $this->sendMessage($this->id, $this->data, $tokens);
                }

                $this->insertUserNotification($details['message']['data']['userIds']);
            }


        } else {
            Log::info("all user");

            $this->sendMessageWithNewKeyAndTopic($this->data);

            $getNonLoggedInUserTokensForIos = RemoteCall::getSpecificWithoutLogin(Config::get("config.oauth_base_url") . "/device/token?user_type=$this->role&country_id=$this->countryId&device_type=ios");
            Log::info("token_request_for_custom_notification_non_logged_in_user",["request" =>$request ,$details,"role" => $this->role]);
            if ($details['status'] != 200) {
                return false;
            }
            if (count($getNonLoggedInUserTokensForIos['message']['data']['tokens']) > 0) {
                $split_array_chunks = array_chunk($details['message']['data']['tokens'],900);
                foreach ($split_array_chunks as $key => $tokens){
                    $this->sendMessage($this->id, $this->data, $tokens);
                }
            }

        }


        return true;
    }

//        }
//    }


    public function sendMessage($id, $data, $token)
    {

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);

        if ($data['action'] == 'new-restaurant') {
            $branchId = $data['meta_data']['branch_id'];

        } else {
            $branchId = '';
        }

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['content'])
            ->setTag(1)
            ->setTitle($data['title'])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData([
            'branch_id' => $branchId,
            'country_id' => $data['country_id'],
            'type' => $data['action'],
        ]);

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
//        dd($downstreamResponse);
        Log::info("old_fcm_v1_custom_notification",["token" => $token,"data" => $data,"response" => $downstreamResponse ]);

        return true;
    }

    private function encodeString($data)
    {
        $key = "Alice123$";
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return $encryptedString = base64_encode($encrypted . '::##::' . $iv);
    }


    /**
     * Insert notification data of user into user_notifications.
     * @return bool
     */
    private function insertUserNotification($userIds)
    {
        try {
            $now = Carbon::now()->toDateTimeString();

            $i = 0;
            foreach ($userIds as $userId) {

                $create[$i]['user_id'] = $userId;
                $create[$i]['title'] = $this->title;
                $create[$i]['data'] = serialize($this->data);
                $create[$i]['status'] = '0';
                $create[$i]['description'] = $this->content;
                $create[$i]['created_at'] = $now;
                $create[$i]['updated_at'] = $now;
                $i++;
            }

            $user = UserNotification::insert($create);
            Log::info("create_user_notification", [  'user_id' => $userId,'data' => $this->data, 'title' => $this->title ]);
            return true;
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
        }

    }

    private function sendMessageWithNewKeyAndToken($data, $tokens)
    {
        $msg = array
        (
            'content' => $data['content'],
            'title' => $data['title'],
            'country_id' => $data['country_id'],
            'type' => $data['action'],
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
        if ($data['action'] == 'new-restaurant') {
            $msg['branch_id'] = $data['meta_data']['branch_id'];
        }
        $fields = array
        (
            'registration_ids' => $tokens,
            "priority" => "high",
            "content_available" => true,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . Config::get('fcm.http.server_key1'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);

        Log::info("custom_push_notification_v2_sendMessageWithNewKeyAndToken", [  'content' => $fields,'headers' => $headers, 'response' => $result ]);
        curl_close($ch);
        return true;
    }


    private function sendMessageWithOldStyleWithTopic($data){
//        $optionBuilder = new OptionsBuilder();
//        $optionBuilder->setTimeToLive(60 * 20);

        if ($data['action'] == 'new-restaurant') {
            $branchId = $data['meta_data']['branch_id'];

        } else {
            $branchId = '';
        }

        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['content'])
            ->setTag(1)
            ->setTitle($data['title'])
            ->setSound('default');

        $dataBuilder = new PayloadDataBuilder();

        $dataBuilder->addData([
            'branch_id' => $branchId,
            'country_id' => $data['country_id'],
            'type' => $data['action'],
        ]);

//        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $topic = new Topics();
        $topic->topic($this->role);
        $downstreamResponse = FCM::sendToTopic($topic, null, $notification, null);
//        $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);
//        dd($downstreamResponse);
        Log::info("old_fcm_v1_custom_notification_topic",["data" => $data,"response" => $downstreamResponse ]);

        return true;
    }

    private function sendMessageWithNewKeyAndTopic($data)
    {

        $msg = array
        (
            'content' => $data['content'],
            'title' => $data['title'],
            'country_id' => $data['country_id'],
            'type' => $data['action'],
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1,
            'largeIcon' => 'large_icon',
            'smallIcon' => 'small_icon'
        );
        if ($data['action'] == 'new-restaurant') {
            $msg['branch_id'] = $data['meta_data']['branch_id'];
        }
        $fields = array
        (
            "condition" => "'$this->role' in topics",
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . Config::get('fcm.http.server_key1'),
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        Log::info("custom_push_notification_v2_sendMessageWithNewKeyAndTopic", [ "data" => $data, 'content' => $fields,'headers' => $headers, 'response' => $result,"status_code" => $httpcode ]);
        curl_close($ch);
        return true;
    }

}