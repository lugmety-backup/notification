<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/29/17
 * Time: 3:45 PM
 */

namespace App\Http\Controllers\NotificationClass;


interface SupportedMethodInterface
{
    public function orderService();

    public function restaurantService();

    public function oauthService();


}