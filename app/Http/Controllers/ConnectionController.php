<?php

namespace App\Http\Controllers;

use App\Repo\SettingsInterface;
use Bschmitt\Amqp\Facades\Amqp;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redis;
use LogStoreHelper;
use Validator;
use Cocur\Slugify\Slugify;

/**
 * Class SettingsController
 * @package App\Http\Controllers
 */
class ConnectionController extends Controller
{

    public function __construct()
    {

    }

    public function index()
    {
        // Test database connection
        $payload = [];
        $flag = true;
        try {
            DB::connection()->getPdo();
            $payload['database']['status'] = true;
            $payload['database']['message'] = "Database is online.";
        } catch (\Exception $e) {
            $flag = false;
            $payload['database']['status'] = false;
            $payload['database']['message'] = "Database is offline.";
        }
        // Test redis connection
        try {
            $redis = Redis::connection();
            $redis->ping();
            $payload['redis']['status'] = true;
            $payload['redis']['message'] = "Redis is online.";

        } catch (\Exception $e) {
            $flag = false;
            $payload['redis']['status'] = false;
            $payload['redis']['message'] = "Redis is offline.";
        }

        // Test rabbitmq connection
        try {
            \Amqp::publish('checker', "hello world", [
                'queue' => 'status_checker_queue',
                'exchange_type' => 'direct',
                'exchange' => 'amq.direct',
            ]);
            $payload['rabbitmq']['status'] = true;
            $payload['rabbitmq']['message'] = "Rabbitmq is online.";

        } catch (\Exception $e) {
            $flag = false;
            $payload['rabbitmq']['status'] = false;
            $payload['rabbitmq']['message'] = "Rabbitmq is offline.";
        }
        if($flag == true){
            return response()->json([
                'status' => '200',
                'data' => $payload
            ], 200);
        }else{
            return response()->json([
                'status' => '503',
                'data' => $payload
            ], 503);
        }
    }


}
