<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 1/11/18
 * Time: 4:43 PM
 */

namespace App\Http\Controllers;

use App\Repo\EventInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class eventController extends Controller
{
    private $event;

    public function __construct(EventInterface $event)
    {
        $this->event = $event;

    }

    public function index(Request $request)
    {
        try {
            $sortBy = $request->get("sort_by", "desc");
            $sortField = $request->get("sort_field");
            $limit = $request->get("limit", 10);
            try {
                $this->validate($request, [
                    "filter_field" => "sometimes|string",
                    "filter_value" => "required_with:filter_field|string",
                    "q" => "sometimes",
                ]);
            } catch (\Exception $ex) {
                Log::error("Event List Display", [
                    "status" => "422",
                    "message" => serialize($ex->response->original),
                    "request" => serialize($request->all())
                ]);

                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }

            try {
                $this->validate($request, [
                    "limit" => "required|integer|min:1"
                ]);
            } catch (\Exception $ex) {
                $limit = 10;
            }

            $parameter = $request->all();
            $parameter["sort_by"] = $sortBy;
            $parameter["sort_field"] = $sortField;
            $parameter["limit"] = $limit;
            $path = '/admin/event';

            $data = $this->event->getAllWithParam($parameter, $path);

            if (count($data) == 0) {
                Log::info("Event List Display", [
                    "status" => "204",
                    "message" => "No record found",
                    "request" => serialize($request->all())
                ]);
                return response()->json([
                    "status" => "204",
                    "message" => "No record found"
                ], 204);
            }
            Log::info("Event List Display", [
                "status" => "200",
                "data" => serialize($data),
                "request" => serialize($request->all())
            ]);
            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        }catch (\Exception $ex) {
            Log::error("Event List Display", [
                'status' => "500",
                'message' => $ex->getMessage(),
                "request" => serialize($request->all())
            ]);


            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }


    }

    /**
     *
     * Store Event
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */


    public function store(Request $request)
    {

        try {
            $this->validate($request, [
                "title" => "required",
                "slug" => "required|unique:events|alpha_dash"
            ]);
        } catch (\Exception $ex) {

            Log::error("Event Validation", [
                "status" => "422",
                "message" => serialize($ex->response->original),
                "request" => serialize($request->all())
            ]);
            return response()->json([
                "status" => "422",
                "message" => $ex->response->original
            ], 422);
        }

        /**
         * Start DB transaction to ensure the operation is reversed in case not successfully committed.
         *
         */

        try {

            DB::beginTransaction();
            $request['slug'] =  str_slug($request['slug']);

            $event = $this->event->create($request->only('title','slug'));


            DB::commit();
            Log::info("Event Create", [
                "status" => "200",
                "message" => "Event Created Successfully",
                "request" => serialize($request->all())
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Event Created Successfully"
            ], 200);

        } catch (\Exception $ex) {
            Log::error("Event Create", [
                'status' => "500",
                'message' => $ex->getMessage(),
                "request" => serialize($request->all())
            ]);

            return response()->json([
                "status" => "500",
                "message" => "There was error in creating Event"
            ], 500);
        }


    }

    public function adminShow($id)
    {
        try {

            $data = $this->event->getSpecificById($id);

            Log::info("Event Display", [
                "status" => "200",
                "data" => serialize($data),
                "id" => $id
            ]);

            return response()->json([
                "status" => "200",
                "data" => $data
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("Event Display", [
                "status" => "204",
                "message" => "No record found",
                "id" => $id
            ]);
            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);

        } catch (\Exception $ex) {
            Log::error("Event Display", [
                'status' => "500",
                'message' => $ex->getMessage(),
                'id' =>$id
            ]);

            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }


    }

    public function update($id, Request $request)
    {


        /**
         * Start DB transaction to ensure the operation is reversed in case not successfully committed.
         *
         */
        try {
            $event = $this->event->getSpecificById($id);

            try {
                $this->validate($request, [
                    "title" => "required",
                    "slug" => "required|unique:events,slug,$id . id|alpha_dash"

                ]);

            } catch (\Exception $ex) {

                Log::error("Event Update", [
                    "status" => "422",
                    "message" => serialize($ex->response->original),
                    "request" => serialize($request->all())
                ]);
                return response()->json([
                    "status" => "422",
                    "message" => $ex->response->original
                ], 422);
            }



            DB::beginTransaction();

            $event = $this->event->update($id, $request->only('title','slug'));

            DB::commit();

            Log::info("Event Update", [
                'status' => "204",
                'message' => "Event Updated Successfully",
                'request' => $request->all(),
                'id' =>$id
            ]);
            return response()->json([
                "status" => "200",
                "message" => "Event Updated Successfully"
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("Event Update", [
                'status' => "204",
                'message' => "No record found",
                'id' =>$id
            ]);

            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);
        } catch (\Exception $ex) {
            Log::error("Event Update", [
                'status' => "500",
                'user_id' => $ex->getMessage(),
                'id' => $id
            ]);

            return response()->json([
                "status" => "500",
                "message" => $ex->getMessage()
            ], 500);

        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $event = $this->event->getSpecificById($id);

            $this->event->delete($id);

            DB::commit();

            Log::info("Event Delete", [
                'status' => "200",
                'message' => "Event successfully deleted.",
                'id' =>$id
            ]);

            return response()->json([
                "status" => "200",
                "message" => "Event successfully deleted."
            ], 200);

        } catch (ModelNotFoundException $ex) {

            Log::info("Event Delete", [
                'status' => "204",
                'message' => "No record found",
                'id' =>$id
            ]);

            return response()->json([
                "status" => "204",
                "message" => "No record found"
            ], 204);
        } catch (\Exception $ex) {
            Log::error("Event Delete", [
                'status' => "500",
                'message' => $ex->getMessage(),
                'id' =>$id
            ]);

            return response()->json([
                "status" => "500",
                "message" => "Something went wrong"
            ], 500);
        }

    }


}