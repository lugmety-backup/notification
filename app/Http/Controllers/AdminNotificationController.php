<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:42 PM
 */

namespace App\Http\Controllers;


use App\Repo\AdminNotificationInterface;
use App\Repo\AdminNotificationStatusInterface;
use Illuminate\Http\Request;
use RoleChecker;
use LogStoreHelper;
class AdminNotificationController extends Controller
{
    protected $adminNotification;
    protected $adminNotificationStatus;
    protected $log;
    public function __construct(AdminNotificationInterface $adminNotification, AdminNotificationStatusInterface $adminNotificationStatus, LogStoreHelper $log)
    {
        $this->adminNotification = $adminNotification;
        $this->adminNotificationStatus = $adminNotificationStatus;
        $this->log = $log;
    }

    public function getAllNotification(Request $request){
        try {
            $limit = $request->get("limit",10);
            try {
                $this->validate($request, [
                    "limit" => "required|integer|min:1"
                ]);
            }
            catch (\Exception $ex){
                $limit = 10;
            }
            $adminNotificationLists = $this->adminNotification->getAllNotification($limit);
            if(count($adminNotificationLists) == 0) throw new \Exception();
            return response()->json([
                "status" => "200",
                "data" => $adminNotificationLists->withPath("/admin/notifications")
            ],200);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty notification"
            ]);
        }

    }

    public function getSpecificNotification($id){
        try{
            /*
             * get user id
             */
            $userId = RoleChecker::getUser();
            /*
             * get specified list of notification by id
             */
            $specificList = $this->adminNotification->getSpecificNotification($id);
            /*
             * checks if the admin has seen that notification
             */
            $checkUserExist = $this->adminNotificationStatus->getSpecificUserNotificationListByNotificationId($userId,$specificList->id);
            /*
             * if not seen then create in admin notification status i.e admin has seen the notification
             */
            if(!$checkUserExist) {
                $createUserStatus = $this->adminNotificationStatus->createAdminNotificationStatus([
                    "user_id" => $userId,
                    "notification_id" => $specificList->id,
                    "status" => "1"
                ]);
                $this->log->storeLogError(array("Notification viewed by admin", [
                    "status" => "200",
                    "data" => $createUserStatus->toArray()
                ]));
            }
            return response()->json([
                "status" => "200",
                "data" => $specificList
            ]);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => '404',
                "message" => "Notification could not be found"
            ],404);
        }
    }

    public function getAllUserSeenNotification(Request $request){
        try {
            $limit = $request->get("limit",10);
            try {
                $this->validate($request, [
                    "limit" => "required|integer|min:1"
                ]);
            }
            catch (\Exception $ex){
                $limit = 10;
            }
            $userId = RoleChecker::getUser();
            $adminNotificationStatusLists = $this->adminNotificationStatus->getAllUserNotificationSeenList($userId,$limit);
            if(count($adminNotificationStatusLists) ==0){
                throw  new \Exception();
            }
            foreach ($adminNotificationStatusLists as $list){
                $list["notification"] = $list->adminNotification()->first();
                $list->makeHidden(["user_id","status","notification_id"]);
            }
            return response()->json([
                "status" => "200",
                "data" => $adminNotificationStatusLists->withPath("/admin/seen/notification")
            ],200);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Empty Notification"
            ]);
        }
    }

    public function getSpecificUserSeenNotification($id){
        try {
            $userId = RoleChecker::getUser();

            $adminNotificationStatusList = $this->adminNotificationStatus->getSpecificUserNotificationListByNotificationId($userId,$id);
            if(count($adminNotificationStatusList) ==0){
                throw  new \Exception();
            }
            $adminNotificationList[] = $adminNotificationStatusList->adminNotification()->first();
            return response()->json([
                "status" => "200",
                "data" => $adminNotificationList
            ],200);
        }
        catch (\Exception $ex){
            return response()->json([
                "status" => "404",
                "message" => "Notification could not be found"
            ]);
        }
    }



}