<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 5/31/2017
 * Time: 1:49 PM
 */

namespace App\Http\Helpers;

use Excel;
class ExcelExportHelper
{
    private $fontColor;
    public function __construct()
    {
        $this->fontColor="#000";

    }
    public function export( $paymentInfo,$filename){
        $ex = Excel::create($filename, function($excel) use ($paymentInfo) {
            // Generate and return the spreadsheet
            $data=$paymentInfo;
            // dd($data);
            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function($sheet) use ($data) {
//                $sheet->protect('ascc', function(\PHPExcel_Worksheet_Protection $protection) {
//                    $protection->setSort(true);
//                });
                $sheet->setWidth(array(
                    'A'     =>  2,
                    'B'     =>  80,
                    'C'     => 30
                ));
                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Arial'
                    )
                ));
                $sheet->setStyle([
                    'borders' => [
                        'allborders' => [
                            'color' => [
                                'rgb' => 'c4b957'
                            ]
                        ]
                    ]
                ]);
                $sheet->row(2, function ($row) {

                    $row->setFontFamily('Georgia');

                });
                $sheet->row(7, function ($row) {

                    $row->setFontFamily('Georgia');

                });
                $sheet->row(12, function ($row) {

                    $row->setFontFamily('Georgia');

                });
                $sheet->cell("B1",function ($cell) use ($data) {
                    $cell->setFont(array(
                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue(  $data["invoice"]);
                });

                $sheet->cell("B2",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '29',
                        'bold'       =>  false,
                    ));
                    $cell->setFontColor('#ff4000');
//                    $cell->path(public_path('img/lugmety-logo.png'));
                });
                $sheet->cell("B3",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("806, 8th floor, King's Road Tower");
                });

                $sheet->cell("B4",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Jeddah, K.S.A");
                });
                $sheet->cell("B5",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("012-6068847");
                });

                $sheet->cell("B6",function ($cell)use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $date=date('l jS \of F Y ');;
                    $cell->setValue($date);
                });

                $sheet->cell("B7",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '15',
                        'bold'       =>  false,
                    ));
                    $cell->setFontColor("#CCAC00");
                    $cell->setValue("BILL TO");
                });

                $sheet->cell("C7",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '15',
                        'bold'       =>  false,
                    ));
                    $cell->setFontColor("#CCAC00");
                    $cell->setValue("FOR");
                    $cell->setAlignment("top");
                });
                $sheet->mergeCells('C8:C11');
                if(!empty($data["tin"])){
                    $sheet->mergeCells('B8:B10');
                }
                else{
                    $sheet->mergeCells('B11:C11');
                }

//                $sheet->mergeCells('B11:C11');
                $sheet->mergeCells('A1:A24');
                $sheet->mergeCells('C1:C6');
//                $sheet->setMergeColumn(array(
//                    'columns' => array('B','C'),
//                    'rows' => array(
//                        array(2,3),
//                        array(5,11),
//                    )
//                ));
                $sheet->cell("B8",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setValignment('top');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue($data["bill_to"]);
                });

                $sheet->cell("C8",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue($data["for"]);
                    $cell->setValignment('top');
                });

                if(!empty($data["tin"])){
                    $sheet->cell("B11",function ($cell)  use ($data){
                        $cell->setFont(array(

                            'size'       => '12',
                            'bold'       =>  false,
                        ));


//                    $cell->setValignment('top');
                        $html = "<b>"."TIN:"."</b>". $data["tin"];
                        $test = new \PHPExcel_Helper_HTML();
                        $test1 = $test->toRichTextObject($html);
                        $cell->setFontColor($this->fontColor);
                        $cell->setValue($test1);
                    });
                }


                $sheet->cell("B12",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("DESCRIPTION");
                });

                $sheet->cell("C12",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("AMOUNT");
                });

                $sheet->cell("B13",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Total Sales");
                    $cell->setBorder('none', 'none', 'thin', 'none');
                });

                $sheet->cell("C13",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $total_sales= $data["total_sales"];
                    $cell->setValue(" $total_sales");
                    $cell->setAlignment('right');
                });

                $sheet->cell("B14",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Cash on pickup");
                });

                $sheet->cell("C14",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cashOnPickup = $data["cash_on_pickup"];
                    $cell->setValue(" $cashOnPickup");
                    $cell->setAlignment('right');
                });
                $sheet->cell("B15",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Cash on delivery");
                });

                $sheet->cell("C15",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cashOnPickup = $data["cash_on_delivery"];
                    $cell->setValue(" $cashOnPickup");
                    $cell->setAlignment('right');
                });
                $sheet->cell("B16",function ($cell) use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Online Card payments");
                });

                $sheet->cell("C16",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $onlineCardPayments = $data["online_card_payments"];
                    $cell->setValue(" $onlineCardPayments");
                    $cell->setAlignment('right');
                });
                $sheet->cell("B17",function ($cell)  use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Sadad");
                });

                $sheet->cell("C17",function ($cell) use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    if(!isset($data["sadad"])){
                        $sadad=0;
                    }
                    else{
                        $sadad = $data["sadad"];
                    }
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue(" $sadad");
                    $cell->setAlignment('right');
                });


                $sheet->cell("B18",function ($cell) use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));

                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Subtotal Less cash on pickup");
                    $cell->setAlignment('right');
                });

                $sheet->cell("C18",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $subtotal = $data["subtotal"];
                    $cell->setValue(" $subtotal");
                    $cell->setAlignment('right');
                });

//                $sheet->cell("B17",function ($cell)  use ($data){
//                    $cell->setFont(array(
//
//                        'size'       => '12',
//                        'bold'       =>  false,
//                    ));
//                    $cell->setFontColor($this->fontColor);
//                    $cell->setValue("Sadad");
//                    $cell->setAlignment('right');
//                });
//
//                $sheet->cell("C17",function ($cell) use ($data){
//                    $cell->setFont(array(
//
//                        'size'       => '12',
//                        'bold'       =>  false,
//                    ));
//                    if(!isset($data["sadad"])){
//                        $sadad=0;
//                    }
//                    else{
//                        $sadad = $data["sadad"];
//                    }
//                    $cell->setBorder('none', 'none', 'thin', 'none');
//                    $cell->setFontColor($this->fontColor);
//                    $cell->setValue(" $sadad");
//                    $cell->setAlignment('right');
//                });

                $sheet->cell("B19",function ($cell)use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cardIssuerFeesRate=$data["card_issuer_fees_rate"];
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("CARD ISSUER FEES [$cardIssuerFeesRate%]" );
                    $cell->setAlignment('right');
                });

                $sheet->cell("C19",function ($cell) use ($data){
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $cardIssuer = $data["card_issuer_fees"];
                    $cell->setValue($cardIssuer );
                    $cell->setAlignment('right');
                });

                $sheet->cell("B20",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $lugmety_commission_rate=$data["lugmety_commission_rate"];
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("LUGMETY.COM COMMISSION [$lugmety_commission_rate%]");
                    $cell->setAlignment('right');
                });
                $sheet->cell("C20",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $lugmetyCommision= $data["lugmety_commission"];
                    $cell->setValue($lugmetyCommision);
                    $cell->setAlignment('right');
                });

                $sheet->cell("B21",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $lugmety_commission_rate=$data["lugmety_commission_rate"];
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("LUGMETY.COM COMMISSION HELD IN CASH AT RESTAURANT [$lugmety_commission_rate%]");
                    $cell->setAlignment('right');
                });

                $sheet->cell("C21",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $lugmetyCashRestaurant= $data["lugmety_cash_at_restaurant"];
                    $cell->setValue($lugmetyCashRestaurant );
                    $cell->setAlignment('right');
                });

                $sheet->cell("B22",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  true,
                    ));

                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("TOTAL CREDIT");
                    $cell->setAlignment('right');
                });

                $sheet->cell("C22",function ($cell) use ($data) {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  true,
                    ));
                    $cell->setBorder('none', 'none', 'thin', 'none');
                    $cell->setFontColor($this->fontColor);
                    $totalCredit = $data["total_credit"];
                    $cell->setValue(" $totalCredit");
                    $cell->setAlignment('right');
                });
                $sheet->mergeCells('B24:C24');
                $sheet->cell("B24",function ($cell)  {
                    $cell->setFont(array(

                        'size'       => '10',
                        'bold'       =>  false,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("Make all transfers to Alawwal Bank Account IBAN: SA2650000000010524106001, Prince Sultan Street Branch, Faisal Husam Uddin Hashim Sadagah for Technology Ltd. If you have any questions concerning this invoice.");
                    $cell->setValignment('top');

                });
                $sheet->cell("B25",function ($cell)  {
                    $cell->setFont(array(

                        'size'       => '12',
                        'bold'       =>  true,
                    ));
                    $cell->setFontColor($this->fontColor);
                    $cell->setValue("THANK YOU FOR YOUR BUSINESS!");
                    $cell->setValignment('top');

                });
                $sheet->setHeight(23, 50);
                $sheet->setHeight(24, 24);
                $sheet->setHeight(array(
                    1     =>  28.5,
                    2     =>  70,
                    3      => 15,
                    4 => 15,
                    5 => 15,
                    6 => 31.5,
                    7 => 35.25,
                    8 => 15,
                    9 => 15,
                    10 => 15,
                    11 => 15,
                    12 => 31.5,
                    13 => 31.5,
                    14 => 31.5,
                    15 => 31.5,
                    16 => 27,
                    17 => 27,
                    18 => 27,
                    19 => 27,
                    20 => 27,
                    21 =>27,
                    22 =>27,
                    23 => 15,
                    24 => 51,
                    25=> 27


                ));
                $sheet->getStyle('C8')->getAlignment()->setWrapText(true);
                $sheet->getStyle('B24')->getAlignment()->setWrapText(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing();
                $objDrawing->setPath(public_path('img/main-logo-height.png')); //your image path
                $objDrawing->setCoordinates('B2');
                $objDrawing->setWorksheet($sheet);
            });

        })->download("xls",false,true);
    }

    public function createSalesReport($orders,$total,$filename){
        $fileFullPath = "sales_report_".$filename;
        Excel::create($fileFullPath, function ($excel) use ($orders, $total) {
            $excel->sheet('mySheet', function ($sheet) use ($orders, $total) {
                $sheet->fromArray($orders);
                $sheet->cell("A1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Order ID");
                });
                $sheet->cell("B1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Order Date");
                });
                $sheet->cell("C1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Restaurant");
                });

                $sheet->cell("D1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Customer");
                });
                $sheet->cell("E1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Customer Note");
                });

                $sheet->cell("F1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Delivery Type");
                });

                $sheet->cell("G1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Payment Method");
                });
                $sheet->cell("H1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Delivery Status");
                });

                $sheet->cell("I1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Order Status");
                });

                $sheet->cell("J1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Payment Status");
                });

                $sheet->cell("K1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Device Type");
                });


                $sheet->cell("L1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Currency");
                });
                $sheet->cell("M1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Sub Total");
                });

                $sheet->cell("N1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Commission");
                });

                $sheet->cell("O1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Delivery Fee");
                });

                $sheet->cell("P1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Tax Amount");
                });
                $sheet->cell("Q1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Grand Total");
                });
                $sheet->cell("R1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Note");
                });

                $sheet->cell("S1", function ($cell) use ($orders) {
                    $cell->setFont(array(

                        'size' => '10',
                        'bold' => true,
                    ));

                    $cell->setValue("Driver");
                });

                $sheet->rows(array(
                    array(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null),
                    array("Total", null, null, null, null, null, null, null, null, null, null, $total[5], $total[1], $total[3], $total[0], $total[4], $total[2], null)
                ));


            });
        })->save('csv',false,true);
//        $total = [
//            $delivery_fee_sum, => 0
//            $sub_total_sum,       1
//            $grand_total_sum,       2
//            $commission_sum,        3
//            $tax_sum,               4
//            $ordersForCsv[0]['currency']     5

//        ];
    }
}