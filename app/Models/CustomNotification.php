<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 1:15 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class CustomNotification extends Model
{
    public $timestamps = true;

    protected $table = 'custom_notifications';

    protected $fillable = ['title','content','meta_data','event_id','sent','user_role', 'country_id', 'targeted_user'];

    public function Event()
    {
        return $this->belongsTo('App\Models\Event','event_id','id');
    }

    public function getCreatedAtAttribute($value)
    {
        if(empty($value)){
            return $value;
        }
        return  Carbon::parse($value)->format('d F Y h:i A');
    }
}