<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/29/17
 * Time: 11:39 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class EmailNotification extends Model
{
    protected $table = 'email_notifications';

    protected $guarded = ['id'];

    public $timestamps = false;
}