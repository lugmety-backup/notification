<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/25/2018
 * Time: 1:35 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class InvoiceNotification extends Model
{
    public $timestamps = true;
    protected $table = 'invoice_notification';

    protected $fillable = ['message','status'];
}