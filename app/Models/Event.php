<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 3/26/18
 * Time: 1:12 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Event extends Model
{
    public $timestamps = false;

    protected $table = 'events';

    protected $fillable = ['title','slug'];
}