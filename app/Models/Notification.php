<?php

/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/3/17
 * Time: 12:20 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Notification extends Model
{
    protected $table = 'notifications';

    protected $fillable = ['message','status'];

}