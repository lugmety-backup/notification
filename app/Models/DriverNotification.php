<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 9/5/17
 * Time: 12:45 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DriverNotification extends Model
{
    protected $table = 'driver_notifications';

    protected $guarded = ['id'];

}