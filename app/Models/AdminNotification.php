<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 9/1/17
 * Time: 3:02 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminNotification extends Model
{
    protected $table = 'admin_notifications';

   protected $fillable = ["title","data"];

   public function getDataAttribute($value){
       return unserialize($value);
   }

   public function adminNotificationStatus(){
    return $this->hasMany('App\Models\AdminNotificationStatus',"notification_id");
   }


}