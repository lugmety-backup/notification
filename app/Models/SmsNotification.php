<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/29/17
 * Time: 11:48 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class SmsNotification extends Model
{
    protected $table = 'sms_notifications';

    protected $guarded = ['id'];

    public $timestamps = false;
}