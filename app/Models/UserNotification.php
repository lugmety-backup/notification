<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 9/1/17
 * Time: 2:55 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UserNotification extends Model
{
    protected $table = 'user_notifications';

    protected $guarded = ['id'];


}