<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 9/9/2017
 * Time: 3:17 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AdminNotificationStatus extends Model
{
    protected $table = 'admin_notification_status';

    protected $fillable = ["user_id","status","notification_id"];

    public $timestamps = false;

    public function adminNotification(){
        return $this->belongsTo('App\Models\AdminNotification',"notification_id");
    }
}