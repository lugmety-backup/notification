<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Models\Notification;

class Listener extends Command
{

    protected $name = 'rabbitmq:listener';
    public function handle()
    {
        \Amqp::consume('notice', function ($message, $resolver) {
            $data=$message->body;
            \Log::info("$data" );

           $this->createNotification($data);

            $resolver->acknowledge($message);
        }, [
            'exchange' => 'amq.direct',
            'exchange_type' => 'direct',
            //'queue_force_declare' => true,
            'queue_exclusive' => false,
            'persistent' => true// required if you want to listen forever1q`
        ]);
    }

    protected function createNotification($data)
    {
        $create['message'] = $data;
        $create['status'] = 'unprocessed';
        Notification::create($create);
        return true;
    }
}
