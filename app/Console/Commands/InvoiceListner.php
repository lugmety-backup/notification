<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/25/2018
 * Time: 12:12 PM
 */

namespace App\Console\Commands;


use Illuminate\Console\Command;
use App\Models\InvoiceNotification;
use Illuminate\Support\Facades\Log;

class InvoiceListner extends Command
{
    protected $name = 'invoice:listener';

    public function handle(){
        \Amqp::consume('invoice', function ($message, $resolver) {
            $data=$message->body;

            $this->createNotification($data);

            $resolver->acknowledge($message);
        }, [
            'exchange' => 'amq.direct',
            'exchange_type' => 'direct',
            //'queue_force_declare' => true,
            'queue_exclusive' => false,
            'persistent' => true// required if you want to listen forever1q`
        ]);
    }


    private function createNotification($data){
        $list = unserialize($data);
        $restaurantsIds = $list["list"];
        $recordToBeInserted = [];
        foreach ($restaurantsIds as $id){
            $tempData = [];
            $tempData["status"] = "unprocessed";

            $tempData["message"] = serialize([
                "service" => "order service",
                "message" => "monthly invoice",
                "data" => [
                    "restaurant_id" => $id,
                    "date" => $list["date"]
                ]
            ]);
            $recordToBeInserted[] = $tempData;
        }
        //create in invoice notification
        InvoiceNotification::insert($recordToBeInserted);


    }
}