<?php
/**
 * Created by PhpStorm.
 * User: Own
 * Date: 2/25/2018
 * Time: 3:24 PM
 */

namespace App\Console\Commands;


use App\Events\SendNotification;
use App\Models\EmailNotification;
use App\Models\InvoiceNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendInvoiceEmail extends Command
{
    protected $name = 'send:invoiceEmail';

    public function handle(){
        while (true) {
            $unprocessedNotifications = InvoiceNotification::where("status", "unprocessed")->get();

            if (!empty($unprocessedNotifications)) {

                foreach ($unprocessedNotifications as $notification) {
                    try{
                        $message = unserialize($notification["message"]);
                        if($this->createEmailNotification($message)){
                            $notification->update([
                                "status" => "processed"
                            ]);
                        };

                        echo ($notification['id']."\n");
                    }catch (\Exception $ex)
                    {
                        InvoiceNotification::find($notification['id'])->update(["status" => "failed"]);
                        continue;
                    }


                }

            }

            sleep(1 * 5);
        }
    }

    private function createEmailNotification( $notification)
    {
        try {
            $create['user_id'] =  '';
            $create['email'] = '';
            $create['subject'] = ucwords($notification["message"]);
            $notification["action"] = str_slug($notification["message"]);
            $create['data'] = serialize($notification);
            $create['template_id'] = 1;

            $data = EmailNotification::create($create);
            $data = $data->toArray();

            $data['notification_type'] = "email";

            event(new SendNotification($data));
            return true;
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            return false;
        }


    }
}