<?php
/**
 * Created by PhpStorm.
 * User: ashish
 * Date: 8/6/2017
 * Time: 3:36 PM
 */

namespace App\Console\Commands;

use App\Events\SendNotification;
use App\Models\EmailNotification;
use App\Models\SmsNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Notification;


/**
 * Class DispatchEmail
 * @package App\Console\Commands
 */
class DispatchEmail extends Command
{
    /**
     * @var string
     */
    protected $name = "send:email";

    public function handle()
    {
        while (true) {
            $unprocessedNotifications = Notification::where("status", "unprocessed")->get();

            if (!empty($unprocessedNotifications)) {

                foreach ($unprocessedNotifications as $notification) {
                    try{
                        $message = unserialize($notification["message"]);

                        $user_details = isset($message['data']['user_details'])?$message['data']['user_details']:'';

                    if ($message['service'] != "oauth service") {
                        if ($this->createEmailNotification($user_details, $message)) {
                            Notification::find($notification['id'])->delete();
                        }

                        } else {
                            if ($this->createEmailNotification($user_details, $message) && $this->createSmsNotification($user_details, $message)) {
                                Notification::find($notification['id'])->delete();
                            }
                        }
                        echo ($notification['id']."\n");
                    }catch (\Exception $ex)
                    {
                        Notification::find($notification['id'])->update(["status" => "failed"]);
                        continue;
                    }


                }

            }

            sleep(1 * 5);
        }

    }

    /**
     * @param $details
     * @param $notification
     * @return bool
     */

    private function createEmailNotification($details, $notification)
    {
        try {
            $create['user_id'] = isset($details['id']) ? $details['id'] : '';
            $create['email'] = isset($details['email']) ? $details['email'] : '';
            $create['subject'] = ucwords($notification["message"]);
            $notification["action"] = str_slug($notification["message"]);
            $create['data'] = serialize($notification);

            $create['template_id'] = 1;

            $data = EmailNotification::create($create);
            $data = $data->toArray();
            $data['notification_type'] = "email";

            event(new SendNotification($data));
            return true;
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            return false;
        }


    }

    /**
     * @param $details
     * @param $notification
     * @return bool
     */
    private function createSmsNotification($details, $notification)
    {
        try {
            $create['user_id'] = isset($details['id']) ? $details['id'] : '';
            $notification['action'] = str_slug($notification['message']);
            $create['data'] = serialize($notification);
            $country_code = $details["country_code"];
            $create['mobile_no'] = isset($details['mobile']) ? $country_code.$details['mobile'] : '';

            $data = SmsNotification::create($create);
            $data = $data->toArray();
            $data['notification_type'] = "sms";

            event(new SendNotification($data));
            return true;
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
            return false;
        }


    }

}