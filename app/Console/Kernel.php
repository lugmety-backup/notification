<?php

namespace App\Console;

use App\Console\Commands\InvoiceListner;
use App\Console\Commands\SendInvoiceEmail;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\Listener',
        'App\Console\Commands\DispatchEmail',
        'App\Console\Commands\FlushRedisListener',
        InvoiceListner::class,
        SendInvoiceEmail::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('send:email')->everyMinute();
    }
}
