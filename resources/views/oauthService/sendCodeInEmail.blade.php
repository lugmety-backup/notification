@extends('master')
@section('content')
    <table class="row mb30" style="border-collapse:collapse;border-spacing:0;display:table;margin-bottom:30px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                            <center data-parsed="" style="min-width:532px;width:100%">
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">
                                    Hello {{trim($user["name"])}} <br>
                                </p>
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">
                                    To finalize your registration with us, you can activate your email by clicking the below link or entering into your browser.</p>
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">
                                   In order to activate your account use the confirmation code provided below:
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important"> <p class="float-center" style="font-size: 15px"><b>Code:{{ $user["url"] }}</b></p></p>
                                <p class="message float-center" align="center" style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important"><b>P.S.</b>  If you did not request to reset your password, please disregard this message. Your account is safe. Please contact info@lugmety.com with any questions. </p>

                            </center>
                        </th>
                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                    </tr>
                </table>
            </th>
        </tr>
        </tbody>
    </table>
@stop

