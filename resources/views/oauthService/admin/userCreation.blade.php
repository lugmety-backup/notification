@extends('master')
@section('content')
    <table class="row mb30"
           style="border-collapse:collapse;border-spacing:0;display:table;margin-bottom:30px!important;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th class="small-12 large-12 columns first last"
                style="Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0 auto;padding:0!important;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px">
                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                    <tr style="padding:0;text-align:left;vertical-align:top">
                        <th style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                            <center data-parsed="" style="min-width:532px;width:100%">
                                <p class="message float-center" align="center"
                                   style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                    Dear Lugmety <br>
                                </p>
                                <p class="message float-center" align="center"
                                   style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                    The username {{trim($user["name"])}} with id #{{$user["id"]}} has been created as a
                                    customer.In order to activate the user use this code below.
                                </p>
                                <p class="message float-center" align="center"
                                   style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                                    The activation code is {{$user["url"]}}.</p>
                            </center>
                        </th>
                        <th class="expander"
                            style="Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                    </tr>
                </table>
            </th>
        </tr>
        </tbody>
    </table>
@stop .
