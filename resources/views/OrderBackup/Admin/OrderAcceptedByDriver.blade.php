@extends('master')

@section('content')

    <div style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
        <center style="max-width:532px;width:100%">
            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:15px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">

                Dear Lumety,<br> Order of id: {{ $user['customer']['order']['id'] }} placed by user {{$user['customer']['name']}} has been assigned to driver {{ $user['customer']['driver']['name'] }}.

            </p>
        </center>
    </div>
    <div class="expander"
         style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0;">
    </div>
    <table style="margin-top:30px; border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left" colspan="2">
                <p class="title" style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">Driver Details</p>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd ">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Name: {{$user['customer']['driver']['name']}}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">Contact No: {{ $user['customer']['driver']['mobile'] }}</p>

            </td>

        </tr>
    </table>
    <table style="margin-top:30px; border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"
                colspan="2">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Order Details</p>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; ">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Order: {{$user['customer']['order']['id']}}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Date Added: {{ $user['customer']['order']['order_date'] }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">

                    Accepted Time: {{ \Carbon\Carbon::now()->toDayDateTimeString() }}

                </p>
            </td>
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-left: 1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Delivery Type: {{ ucwords(str_replace("-", " ",$user['customer']['order']['delivery_type'])) }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Payment
                    Method: {{ ucwords(str_replace("-", " ",$user['customer']['order']['payment_method'])) }}</p>
            </td>
        </tr>
    </table>

    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"
                colspan="2">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Instruction</p>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top ; border-right:1px solid #ddd;">
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #dddddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Order Date: {{$user['customer']['order']['order_date'] }}</p>
            </td>

            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #dddddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Estimated @if($user['customer']['order']['delivery_type'] == 'pickup') Pick Up @else Delivery @endif
                    Time:
                    12 September 2017
                </p>
            </td>

        </tr>
    </table>


    <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">

            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Payment Information</p>
            </th>

            @if($user['customer']['order']['delivery_type'] == 'pickup' && $user['customer']['order']['payment_method'] == 'cash-on-pickup')
                <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd">
                    <p class="title"
                       style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                        Personal Information</p>
                </th>
            @endif
            @if($user['customer']['order']['delivery_type'] != 'pickup')
                <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                    <p class="title"
                       style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                        Delivery Information</p>
                </th>
            @endif
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">

            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['name'] }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['email']}}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['mobile']}}</p>
                @if(isset($user['customer']['order']['extra_info']['shipping_details']))
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['order']['extra_info']['shipping_details']['address_name']}}</p>
                @endif
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['country']." ,".$user['customer']['city']." ,".$user['customer']['address_line_1']}}</p>
                @if(isset($user['customer']['order']['extra_info']['shipping_details']))
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['order']['extra_info']['shipping_details']['zip_code']}}</p>
            </td>
            @endif


            @if($user['customer']['order']['delivery_type']  == 'pickup' && $user['customer']['order']['payment_method']  == 'cash-on-pickup')
                <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $user['customer']['name'] }}</p>
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['email']}}</p>
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['mobile']}}</p>

                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['country']." ,".$user['customer']['city']." ,".$user['customer']['address_line_1']}}</p>
                    @if(isset($user['customer']['order']['extra_info']['shipping_details']))
                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['order']['extra_info']['shipping_details']['street'] }}</p>
                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $user['customer']['order']['extra_info']['shipping_details']['zip_code'] }}</p>
                </td>
            @endif
            @endif

            @if($user['customer']['order']['delivery_type'] != 'pickup')
                <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $user['customer']['name'] }}</p>
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['email']}}</p>
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['mobile']}}</p>

                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$user['customer']['country']." ,".$user['customer']['city']." ,".$user['customer']['address_line_1']}}</p>
                    @if(isset($user['customer']['order']['extra_info']['shipping_details']))
                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $user['customer']['order']['extra_info']['shipping_details']['street'] }}</p>

                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $user['customer']['order']['extra_info']['shipping_details']['zip_code'] }}</p>

                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                            House(Block)
                            No.: {{ $user['customer']['order']['extra_info']['shipping_details']['house_number'] }}</p>
                        @if(!empty($user['customer']['order']['extra_info']['shipping_details']['building_name']))
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                                Bulding
                                Name: {{ $user['customer']['order']['extra_info']['shipping_details']['building_name'] }}</p>
                        @endif
                        @if(!empty($user['customer']['order']['extra_info']['shipping_details']['floor_number']))
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                                Floor
                                Number: {{ $user['customer']['order']['extra_info']['shipping_details']['floor_number'] }}</p>
                        @endif
                        @if(!empty($user['customer']['order']['extra_info']['shipping_details']['office_name']))
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                                Office
                                Name: {{ $user['customer']['order']['extra_info']['shipping_details']['office_name'] }}</p>
                        @endif
                        @if(!empty($user['customer']['order']['extra_info']['shipping_details']['extra_direction']))
                            <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                                Extra
                                Direction: {{ $user['customer']['order']['extra_info']['shipping_details']['extra_direction'] }}</p>
                        @endif
                    @endif
                    @if(!empty($user['customer']['order']['customer_note']))
                        <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                            Customer Note: {{ $user['customer']['order']['customer_note'] }}</p>
                    @endif
                </td>
            @endif
        </tr>
    </table>



    <h3 class="subtitle">{{ $user['customer']['order']['restaurant']['translation']['en'] }}</h3>
    <h5 class="subtitle">Phone No. {{ $user['customer']['order']['restaurant']['phone_no'] }}</h5>
    <table class="footer-table"
           style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Product</p>
            </th>
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Price</p>
            </th>
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Quantity</p>
            </th>
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Total</p>
            </th>
        </tr>
        </thead>
        <tbody>

        <?php $currency_symbol = $user['customer']['order']['currency'];
        $sub_total = 0;
        ?>
        @foreach($user['customer']['order']['extra_info']['items'] as $item)

            <tr style="padding:0;text-align:left;vertical-align:top; border-top:none;">
                <td data-label="Product"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd; border-bottom:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$item['food_details']['food_name']['en']}}</p>
                    <?php $addonsArray = array(); ?>
                    @if(count($item['addons'])>0)
                        @foreach($item['addons'] as $addon)
                            <?php $addonsArray[] = unserialize($addon['addon_name'])['en'] . ' (' . $currency_symbol . ' ' . $addon['unit_price'] . ')'; ?>
                        @endforeach
                    @endif
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ implode(", ",$addonsArray) }}</p>

                </td>

                <td data-label="Price"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd; border-bottom:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $currency_symbol }} {{$item['food_details']['unit_price']}}</p>
                </td>

                <td data-label="Qty"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd; border-bottom:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$item['food_details']['quantity']}}</p>
                </td>

                <td data-label="Total"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd; border-bottom:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                        @php
                            $sub_total +=  $item['food_details']['total_unit_price'] * $item['food_details']['quantity'];
                        @endphp
                        {{ $currency_symbol }}
                        {{ number_format((float)$item['food_details']['total_unit_price'] * $item['food_details']['quantity'] , 2, '.', '')}}
                    </p>
                </td>
            </tr>
        @endforeach


        </tbody>
        <tfoot>
        <tr>
            <td colspan="3"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:right">
                    Sub Total</p>
            </td>
            <td data-label="Sub Total"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border:1px solid #dddddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    @php
                      // $sub_total = ($user['customer']['order']["site_commission_amount"]/$user['customer']['order']["site_commission_rate"]) * 100;
                        $tax = $sub_total*$user['customer']['order']['site_tax_rate']* 0.01;
                    @endphp
                    {{ $currency_symbol }} {{$sub_total}}
                </p>
            </td>
        </tr>
        <tr style="border-top: 1px solid #ddd">
            <td colspan="3"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:right">
                    Tax</p>
            </td>
            <td data-label="Tax"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border: 1px solid #dddddd;">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{ $currency_symbol }} {{  $tax }}</p>
            </td>
        </tr>
        @if($user['customer']['order']['delivery_type'] == 'home-delivery')
            <tr style="border-top: 1px solid #ddd">
                <td colspan="3"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border:1px solid #ddd">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:right">
                        Delivery Charge</p>
                </td>
                <td data-label="Delivery Charge"
                    style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border: 1px solid #dddddd;">
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                        {{ $user['customer']['order']['delivery_fee'] }}
                    </p>
                </td>
            </tr>
        @endif
        {{--@if($user['customer']['order']['payment_method'] == 'creditcard')--}}
            {{--<tr>--}}
                {{--<td colspan="3"--}}
                    {{--style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border:1px solid #ddd">--}}
                    {{--<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:right">--}}
                        {{--Credit Card Service Fee</p>--}}
                {{--</td>--}}
                {{--<td data-label="Credit Card Service Fee"--}}
                    {{--style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border: 1px solid #dddddd;">--}}
                    {{--<p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">--}}
                        {{--{{$currency_symbol}} 10--}}

                    {{--</p>--}}
                {{--</td>--}}
            {{--</tr>--}}
        {{--@endif--}}
        <tr>
            <td colspan="3"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:right">
                    Grand Total</p>
            </td>
            <td data-label="Grand Total"
                style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border: 1px solid #dddddd;">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:12px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">{{$currency_symbol}} {{ $user['customer']['order']['amount']}}</p>
            </td>
        </tr>
        </tfoot>
    </table>

@stop

