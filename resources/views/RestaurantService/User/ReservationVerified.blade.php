@extends('master')

@section('content')
    <div style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
        <center style="max-width:532px;width:100%">
            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">

                Dear {{ trim($user['name']) }}
            </p>
            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">

                {{--@if($user['status'] == "accept")--}}
                {{--<br>--}}
                {{--Thank you for your reservation request. Your reservation has been approved, we look forward to seeing you!<br>--}}

                {{--Below is your reservation schedule:--}}

                {{--@elseif($user['status'] == "reject")--}}

                {{--Thank you for your reservation request, unfortunately we cannot accommodate you at the specified time. We apologize for any inconvenience caused and suggest you try selecting another date/time.<br>--}}
                {{--@endif--}}
                @if($user['status'] == "accept")
                    Thank you for your reservation request. Your reservation has been approved, we are looking forward to
                    see you!<br>
                    Below is your reservation schedule:
                @else
                    Thank you for your reservation request, unfortunately we cannot accommodate you in your preferred
                    timing. We apologize for any inconvenience caused by this. We advise you to try selecting another date/time or other restaurants nearby.<br>
                @endif

            </p>
        </center>
    </div>
    <div class="expander"
         style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0;">
    </div>

    <table style="margin-top:30px; border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"
                colspan="2">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Details</p>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left; border-right:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Reservation Date: {{ $user['booking_date'] }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Reservation Time: {{ $user['booking_time'] }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Number of seats: {{ $user['seats_count']}}</p>

                @if($user['status'] == "accept")

                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                        Accepted Time: {{ $user['updated_at'] }}</p>


                @else
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                        Rejected Time: {{ $user['updated_at'] }}</p>
                @endif
                    <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                        Restaurant: {{ $user['restaurant']['name'] }}</p>
            </td>

        </tr>
        </tbody>
    </table>



@stop


