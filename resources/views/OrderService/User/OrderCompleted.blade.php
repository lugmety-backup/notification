@extends('master')

@section('content')
    <div style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:center">
        <center style="max-width:532px;width:100%">
            <p class="message float-center" align="center" style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">

                Dear {{ trim($user['name']) }}

            </p>

            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                {{--@if($order->payment_method == 'Paytabs')--}}
                {{--Thank you for your interest in our restaurant. Your order has been received and will be processed once payment has been confirmed.--}}
                {{--@else--}}
                {{--Thank you for your interest in our restaurant. Your order has been received and will be processed soon.--}}
                {{--@endif--}}
                Your order #{{$user['order']['id']}} has been completed and delivered successfully. Enjoy your food.
            </p>
        </center>
    </div>


@stop