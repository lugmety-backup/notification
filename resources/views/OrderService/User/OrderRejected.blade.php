@extends('master')

@section('content')
    <div style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:center">
        <center style="max-width:532px;width:100%">
            <p class="message float-center" align="center" style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">

                Dear {{ trim($user['name']) }}

            </p>

            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:center!important">
                @if(isset($user['order']['payment_method']) && ('creditcard' === $user['order']['payment_method']))
                    Unfortunately the restaurant has rejected your order #{{$user['order']['id']}} for some reason. Your refund will be processed shortly, in the meantime feel free to try another restaurant, after all you still need to eat!
                @else
                    Unfortunately the restaurant has rejected your order #{{$user['order']['id']}} for some reason. In the meantime feel free to try another restaurant, after all you still need to eat!
                @endif
            </p>
        </center>
    </div>


@stop

