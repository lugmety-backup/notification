@extends('master')

@section('content')
    <div style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
        <center style="max-width:532px;width:100%">
            <p class="message float-center" align="center"
               style=" margin-bottom:15px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left!important">

                Dear Lugmety<br>

                @if(isset($user['driver']['update']))

                    Payout request for driver #ID:{{ $user['driver']['id']}} has been updated to {{ $user['driver']['payouts']['status'] }}.
                @else

                    Payout amount has been issued for the driver as requested. Please find the details below:
                    . <br>
                @endif


            </p>
        </center>
    </div>
    <div class="expander"
         style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0;">
    </div>
    <table style="margin-top:30px; border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%; border-top: none !important; margin-bottom:30px; border:1px solid #ddd">
        <thead>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <th style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"
                colspan="2">
                <p class="title"
                   style="margin: 0 !important;background:#ece4d6!important;border-bottom:1px solid #ddd;border-top:1px dashed #ddd;font-weight:700;padding:10px!important">
                    Driver Details</p>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr style="padding:0;text-align:left;vertical-align:top">
            <td style="margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;border-right:1px solid #ddd">
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Name: {{ trim($user['driver']['name']) }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Email: {{ $user['driver']['email'] }}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Mobile: {{ $user['driver']['mobile']}}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Amount: {{ $user['driver']['country_info']['currency_symbol'] }} {{ $user['driver']['payouts']['amount']}}</p>
                <p style="Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Montserrat,sans-serif!important;font-size:14px!important;font-weight:400!important;line-height:1.3;margin:0!important;margin-bottom:10px;padding:5px 20px!important;text-align:left">
                    Status: {{ ucwords($user['driver']['payouts']['status'])}}

                </p>
            </td>

        </tr>
        </tbody>
    </table>


@stop



