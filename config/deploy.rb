# config valid only for current version of Capistrano
lock "3.10.1"

set :application, "lugmety_restaurant"
set :repo_url, 'git@gitlab.com:lugmety/notifications.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
