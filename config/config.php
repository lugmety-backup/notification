<?php
/**
 * Created by PhpStorm.
 * User: hazesoft_one
 * Date: 8/31/17
 * Time: 11:36 AM
 */
if(env("APP_ENV") == "production"){
    return array(

        'url' => 'https://api.lugmety.com/auth/v1/oauth/introspect',

        'twilio_number' => env('TWILIO_NUMBER'),

        "oauth_base_url" =>  'https://api.lugmety.com/auth/v1',

        "restaurant_base_url" => 'https://api.lugmety.com/restaurant/v1',

        "settings_url"=>"https://api.lugmety.com/general/v1/public/settings/",

        "cdn_base_url"=>"https://api.lugmety.com/cdn/v1",
    );
}
else{
    return array(

        'url' => 'http://api.stagingapp.io/auth/v1/oauth/introspect',
        //'url' => 'http://localhost:8888/oauth/introspect',

        'twilio_number' => env('TWILIO_NUMBER'),

        "restaurant_base_url" => 'http://api.stagingapp.io/restaurant/v1',

//    'restaurant_base_url' => 'http://localhost:9999',

        "oauth_base_url" =>  'http://api.stagingapp.io/auth/v1',
        //"oauth_base_url" =>  'http://localhost:8888'
        "settings_url"=>"http://api.stagingapp.io/general/v1/public/settings/",

        "cdn_base_url"=>"http://api.stagingapp.io/cdn/v1",
    );
}
