<?php
return array(
    'driver' => 'http',
    'log_enabled' => false,
    'http' =>
        array(
            'server_send_url' => 'https://fcm.googleapis.com/fcm/send',
            'server_group_url' => 'https://android.googleapis.com/gcm/notification',
            'timeout' => 30.0,
            'server_key' => env('FCM_SERVER_KEY'),
            'sender_id' => env('FCM_SENDER_ID'),
            'server_key1' => env('FCM_SERVER_KEY1'),
            'sender_id1' => env('FCM_SENDER_ID1'),
        ),
);

