<?php
use App\Http\Controllers\NotificationClass\EmailNotification;
use App\Http\Controllers\NotificationClass\SmsNotification;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/check/new/{token}', 'ExampleController@testnotification'
  );

$app->get('/check/old/{token}', 'ExampleController@testFsm'
);



$app->group(['notifications user group'], function () use ($app) {
    /**
     * Get list of payment methods for admin
     */
    $app->get('user/notifications', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'NotificationController@getAllUserNotifications'
    ]);

    $app->get('user/notifications/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'NotificationController@getSpecificUserNotification'
    ]);

    $app->put('user/notifications/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'NotificationController@updateUserNotification'
    ]);
    $app->get('user/unseen-notifications', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'NotificationController@getUnseenNotification'
    ]);

});

$app->group(['notifications admin group'], function () use ($app) {
    /**
     * Get list of payment methods for admin
     */
    $app->get('admin/notifications', [
        'middleware' => 'checkTokenPermission:view-admin-notification',
        'uses' => 'AdminNotificationController@getAllNotification'
    ]);

    $app->get('admin/notification/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:view-admin-notification',
        'uses' => 'AdminNotificationController@getSpecificNotification'
    ]);

    $app->get('admin/seen/notification', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'AdminNotificationController@getAllUserSeenNotification'
    ]);

    $app->get('admin/seen/notification/{id:[0-9]+}', [
        'middleware' => 'checkTokenPermission:view-admin-notification',
        'uses' => 'AdminNotificationController@getSpecificUserSeenNotification'
    ]);

});

$app->group(['notifications driver group'], function () use ($app) {
    /**
     * Get list of payment methods for admin
     */
    $app->get('driver/notifications', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverNotificationController@getAllUserNotifications'
    ]);

    $app->get('driver/notifications/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverNotificationController@getSpecificUserNotification'
    ]);

    $app->put('driver/notifications/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverNotificationController@updateUserNotification'
    ]);

    $app->get('driver/unseen-notifications', [
        'middleware' => 'checkTokenPermission',
        'uses' => 'DriverNotificationController@getUnseenNotification'
    ]);
});


$app->get('connection/checker', 'ConnectionController@index');

$app->get('redis/slug/{slug}', function ($slug) use ($app) {
    return \Redis::get($slug);
});

$app->group(['event group'], function () use ($app) {

    $app->get('admin/event', [
        'middleware' => 'checkTokenPermission:view-admin-event',
        'uses' => 'EventController@index'
    ]);

    $app->get('admin/event/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:view-admin-event',
        'uses' => 'EventController@adminShow'
    ]);

    $app->post('admin/event', [
        'middleware' => 'checkTokenPermission:create-admin-event',
        'uses' => 'EventController@store'
    ]);

    $app->delete('admin/event/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:delete-admin-event',
        'uses' => 'EventController@delete'
    ]);

    $app->put('admin/event/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:update-admin-event',
        'uses' => 'EventController@update'
    ]);
});


$app->group(['custom notification group'], function () use ($app) {

    $app->get('admin/custom-notification', [
        'middleware' => 'checkTokenPermission:view-custom-notification',
        'uses' => 'CustomNotificationController@index'
    ]);

    $app->get('admin/custom-notification/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:view-custom-notification',
        'uses' => 'CustomNotificationController@adminShow'
    ]);

    $app->post('admin/custom-notification', [
        'middleware' => 'checkTokenPermission:create-custom-notification',
        'uses' => 'CustomNotificationController@store'
    ]);

    $app->delete('admin/custom-notification/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:delete-custom-notification',
        'uses' => 'CustomNotificationController@delete'
    ]);

    $app->put('admin/custom-notification/{id: [0-9]+}', [
        'middleware' => 'checkTokenPermission:update-custom-notification',
        'uses' => 'CustomNotificationController@update'
    ]);

    $app->put('admin/custom-notification/trigger/{id: [0-9]+}',[
        'middleware' => 'checkTokenPermission:trigger-push-notification',
        'uses' => 'CustomNotificationController@trigger'
    ]);
});

$app->get('test/email', 'ExampleController@test');
$app->get('test/fsm', 'ExampleController@testFsm');