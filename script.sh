#!/bin/bash

### kill all running processes
kill $(ps aux | grep '[p]hp' | awk '{print $2}')

### new processes
nohup php /var/www/app/artisan rabbitmq:listener &
nohup php /var/www/app/artisan queue:work &
nohup php /var/www/app/artisan send:email &
nohup php /var/www/app/artisan rabbitmq:flushredislistener &

